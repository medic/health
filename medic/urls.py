# -*- coding: utf-8

from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from medic import views

# Definición de URLs para Medic

urlpatterns = patterns('',
    url(r'^$', views.inicio, name="medic_root" ), 
    url(r'^nosotros/$', TemplateView.as_view(template_name="nosotros.html"), name="we"),  

)