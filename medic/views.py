# *.* coding:utf-8 *.*

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.db.models import Q
from django.conf import settings

from datamark.models import Establecimiento, Doctor
from datamark.consts import *

def inicio(request):	
	"""Página de inicio de Medic"""

	# Numeros
	n_doctores = Doctor.objects.all().count()
	n_clinicas = Establecimiento.objects.filter(tipo='CL').count()
	n_establecimeintos = Establecimiento.objects.count()

	n_total = n_doctores + n_establecimeintos

	# Configuraciones para Medic

	confs = {}

	mse_host = settings.MSE['host']

	if (settings.MSE['port'] and settings.MSE['port'] != ""):
		mse_port = ":" + str(settings.MSE['port'])

	confs['mse'] = settings.MSE
	
	confs['urls'] = {}
	confs['urls']['media'] = settings.MEDIA_URL 

	# Constantes compartidas

	consts = {}

	# Generarse a partir de tuplas

	consts['sexos'] = {}
	consts['sexos']['masculino'] = MASCULINO
	consts['sexos']['femenino'] = FEMENINO

	consts['titulos'] = {}
	consts['titulos']['doctor'] = DOCTOR
	consts['titulos']['licenciado'] = LICENCIADO
	consts['titulos']['psicologo'] = PSICOLOGO

	consts['tipos_entidades'] = {}
	consts['tipos_entidades']['doctor'] = 'D'
	consts['tipos_entidades']['establecimiento'] = 'E'

	request.session.modified = True

	return render_to_response('inicio.html', 
		{ 'n_doctores' : n_doctores,
		  'n_clinicas' : n_clinicas,		  
		  'n_total': n_total,
		  'confs': confs,
		  'consts': consts,
		  'mse_host': mse_host,
		  'mse_port': mse_port
		}, 
		context_instance=RequestContext(request))


def perfil(request, user):

	doctor = None
	establecimiento = None
	template = None
	entity = None

	try:
		doctor = Doctor.objects.get(usuario=user)		
	except: 
		pass

	if doctor:
		template = 'doctor_profile.html'
		entity = doctor
	else:
		establecimiento = get_object_or_404(Establecimiento, usuario=user)
		template = 'institution_profile.html'
		entity = establecimiento	

	return render_to_response(template, {'entity': entity}, context_instance=RequestContext(request))


def busqueda(request):

	return render_to_response('busqueda_form.html', {}, context_instance=RequestContext(request))	

def resultados_busqueda(request):
	if 'clave' in request.GET :
		p = request.GET['clave']
		palabras = p.split(',')
		consulta = Establecimiento.objects
		q= Q()
		for elemento in palabras:
			q = q |  Q(servicios__icontains=elemento)
		consulta = consulta.filter(q)
		
	return render_to_response('resultado_busqueda.html',{'establecimientos':consulta},context_instance=RequestContext(request))	
