# Health

Sistema interno de variados propósitos para Proyecto Medic.

## Módulos

  * **Base** - Funcionalidades base de Health

  * **Datamark** - Sistema de Análisis del Mercado.

  * **Pollmark** - Sistema de Apoyo para Encuestas del Mercado.

## Instalacion del proyecto

Para la instalaciónde Django debe usarse el fork para el soporte de base de datos no_sql, para ello correr el siguiente comando

```bash
   pip install git+git://github.com/django-nonrel/django@nonrel-1.5-beta
```
Luego debe instalarse el resto de los paquetes, los cuales se encuentran en requeriments.txt, para ello correr

```bash
   pip install -r requeriments.txt
```

Realizar una copia del archivo en la carpeta health llmado 'sample_local_settings.py' y colocarle el nombre de 'local_settings.py'. Colocar en este archivo las configuraciones de las base de datos. Si no se posee mongodb, dejar la configuración que ya posee.

En en el caso de la base de datos SQL, importar el script medic_health.sql ya que posee datos reales. En caso de querer una instalación desde cero, seguir los siguiente pasos.

Realizar la sincronización de la base de datos, pero sin hacer crear un super usuario, es decir, cuando pregunte si se desea crear un superusuario, indicar que no. Si se dice que si dará un error, ya que la tabla de usuarios depende de las tablas mantenidas por south (que se crean con el migrate no con el syncdb).

```bash
   python ./manage.py syncdb
```
Luego hacemos el migrate de todas las tablas de todos los módulos de health.


```bash
   python ./manage.py migrate
```
Para finalizar creamos un superusuario, el cual nos pedirá nombre, correo y clave.

```bash
   python ./manage.py createsuperuser
```

## Hacer funcionar el servidor

Primero debes encontrarte en la carpeta health. Llega hasta allí con el comando cd.
La base de datos local debe llamarse health.

Ejecutar

```bash
   python ./manage.py runserver
```

Para opciones mas avanzadas de debugging usar


```bash
   python ./manage.py runserver_plus
```