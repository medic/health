var Comentario = Backbone.Model.extend({	

	urlRoot: 'health/pollmark/api/comentario/',

	defaults: {
		'conformidad': '',
		'detalle_sitio': '',
		'comentario': ''
	}

});



var ModalView = Backbone.View.extend({

el: "#modal",

events: {
	"change #id_conformidad" : "respuesta",	
	"click #btn-guardar-opinion" : "guardarEncuesta"

},		

respuesta : function(event){
	/*Mostrar modal con la pregunta*/	
	console.log("Cambio a la siguiente pregunta")
	console.log(event.currentTarget)

	element = $(event.currentTarget)
	this.conformidad.set("N/A")
	//$("#pregunta2").addClass("no-display")
	$("#pregunta3").removeClass("no-display")

},

guardarEncuesta : function(){
	/*Mostrar modal con la pregunta*/
	this.comentario.set({comentario:$("#id_comentario").val()})
	console.log("almaceno la encuesta ->"+this.comentario.get)
	this.comentario.save()
	$.cookie("encuesta","si")
	$("#widget-encuesta").addClass("hidden")
	$('#modal').foundation('reveal', 'close');

},

showModal: function(){
	$("#modal").foundation('reveal', 'open');
	$("#widget-encuesta").removeClass("desplegar")
	$("#widget-encuesta").addClass("ocultar")
	console.log("Mostrar Modal")
},

hideModal: function(){
	$('#modal').modal('hide')
	console.log("ocultar modal")
},

initialize : function(options){
	this.comentario = options.comentario
},

});



var AlertEncuestaView = Backbone.View.extend({

el: "#widget-encuesta",

events: {
	"click .opcion" : "clickOpcion"
},		

clickOpcion : function(e){
	/*Mostrar modal con la pregunta*/	
	this.modal.showModal()
	elemento = $(e.currentTarget)
	this.comentario.set({detalle_sitio:elemento.attr("valor")})
	
	console.log("Respondio, se muestra mnodal!")
},

initialize : function(options){
	this.modal = options.modal
	this.comentario = options.comentario
	if ($.cookie("encuesta")) {
		/* Mostar el alert con las caitas */
		setTimeout( function(){
			console.log("Mostrando aviso.")
			mostrar_aviso('¿Le gusto el sitio? <br> <button class="btn opcion" id="btn-opinion" type="button" valor="SI">Si</button> <button class="btn opcion" id="btn-opinion" type="button" valor="NO">No</button>')
		   
		},5000)
	}
	//};
},
//var BuscardorInput = new BuscardorInputView({resultsView: ResultsContainer} );	
});
$(document).ready(function() {
var comentario = new Comentario();
var modalview = new ModalView({comentario:comentario});
var alert_encuesta = new AlertEncuestaView({modal: modalview,comentario:comentario});
})