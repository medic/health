/*
	medic.js

	Realiza las llamadas para la búsqueda y gestiona la interfaz para la presentación de estas.

*/

$(function(){	

	// Especifica si se observando desde un dispositivo móvil	

	var SocketListenerView = Backbone.View.extend({

		initialize: function(){
			this.__initialize();
		},

		__initialize: function(){
			if (this.socket_events && _.size(this.socket_events) > 0) {
            	this.delegateSocketEvents(this.socket_events, this.socket);
        	}
		},

		delegateSocketEvents: function (events, socket) {
	        for (var key in events) {
	            var method = events[key];
	            if (!_.isFunction(method)) {
	                method = this[events[key]];
	            }
	 
	            if (!method) {
	                throw new Error('Method "' + events[key] + '" does not exist');
	            }
	 
	            method = _.bind(method, this);
	            socket.on(key, method);
	        };
	    }
	});


	// Modelos	

	var EntityModel = Backbone.Model.extend({
		defaults: {			
			entity_type: '',
			ci: 0,
			nombre: '',
			usuario: '',
			servicios: '',					
			web: ''
		},			

		initialize: function(){

			if (this.get('web') == '')
				this.set('web', 'medic.com.ve/' + this.get('usuario'));
			else
				this.set('web', 'www.' + this.get('web'));

			this.generateReadableServices();
		},

		isDoctor: function(){
			return this.get('entity_type') == Medic.consts.TIPOS_ENTIDADES.DOCTOR;
		},

		isJoined: function(){
			return this.get('afiliado') == true;
		},

		generateReadableServices: function(){
			var servicios = "";
			var servicios_array = this.get('servicios_array');
			
			if (servicios_array){
				var nServices = servicios_array.length;

				for (var i = 0; i < nServices; i++) {

					servicios_array[i] = servicios_array[i].replace(",", ":")
					
					servicios += servicios_array[i];

					if (i+1 != nServices)
						servicios += " - "
				};	
			}
			
			this.set('servicios', servicios);
		}

	});

	var EntityCollection = Backbone.Collection.extend({
		model: EntityModel,		
	});
	var EntitiesResult = new EntityCollection([]);

	// Vistas

	var MedicView = Backbone.View.extend({
		el: 'body',
		initialize: function(){			

			this.loadSharingData("confs");  	// Carga de configuraciones 
			this.loadSharingData("consts");		// Carga de constantes compartidas

			var mse_interface_uri = 'http://' + this.confs.MSE.HOST + ':' + this.confs.MSE.PORT;

    		// Variable para el socket a MSE
			this.mse_socket       = io.connect(mse_interface_uri); 
			this.mobile           = (window.innerWidth <= 768) ? true : false;
			this.media_url        = $("#media_url").attr("data-media");

			// Variables para diseños de tarjetas

			this.max_doctor_headers = 4;		

			this.totalResults = $("#total_results").html();

			// Códigos de respuesta del MSE
			this.MSEResponses = {};
			this.MSEResponses.NOT_RELEVANT_INFO = 3;
			this.MSEResponses.WITH_RESULTS = 1;
			this.MSEResponses.WITHOUT_RESULTS = 2;
			this.MSEResponses.RESULTS_RESTORED = 4;
			
			// Constantes manejadas por Health y el MSE			

			this.templates = {};
			this.templates.card_views = {};
			this.templates.card_views.doctor_joined = $("[data-template-locator=doctor_card_joined]").html();
			this.templates.card_views.doctor = $("[data-template-locator=doctor_card]").html();
			this.templates.card_views.institution = $("[data-template-locator=institution_card]").html();
			this.templates.card_views.institution_joined = $("[data-template-locator=institution_card_joined]").html();

			// Cacheando todos los elementos a manipular

			this.$resultsTopbarElement = $("#results-topbar");
			this.$resultsTopbarMessageElement = $("#results-message");
			this.$loaderElement = $("#loader");
			this.$presentationElement = $("#entry");			

		},

		loadSharingData: function(key_type){	

			var data = {};		

			var singular_name = key_type.substr(0, key_type.length-1);

			_.each( $("#" + key_type + "  > div"), function(conf_node){

				var conf_key= conf_node.dataset[singular_name+'Name'].toUpperCase();					

				 var child_properties = {};

				_.each($("[data-" + singular_name +"-name="+ conf_key.toLowerCase() +"] > div"), function(node){

					child_properties[node.dataset[singular_name+'Key'].toUpperCase()] = node.dataset[singular_name + 'Value'];

				});

				data[conf_key] = child_properties;
				
							
			});			

			this[key_type] = data;					

		},


		// Cambios en la interfaz para mostrar el cargador entre búsquedas
		showLoader: function(){
			$("#presentacion").addClass("hide");
			this.$resultsTopbarElement.hide();
			this.$resultsTopbarElement.removeClass("show-filtros");

			this.$loaderElement.removeClass("hide");
			this.$loaderElement.addClass("show-anim");
		},

		// Prepara el lugar para mostrar los resultados
		prepareForShowResults: function(){
			this.$loaderElement.addClass("hide");
			this.$loaderElement.removeClass("show-anim");
			this.$resultsTopbarElement.show();
			this.$resultsTopbarElement.addClass("show-filtros");			
		},

		preSearch: function(){
			// Acciones de cambio de interfaz que se realizaran antes de iniciar la búsqueda.

			this.$presentationElement.addClass("hide-entry");
			Medic.showLoader();	
		},

		preRecovery: function(){
			Medic.showLoader();	
			this.$presentationElement.addClass("hide");
		},

		updateResultsTopbarMessage: function(resultsCount){

			var message  = "";

			if (resultsCount > 0)
				message = "Se encontraron " + resultsCount + " resultados para su consulta";
			else
				message = "No se encontraron resultados para su consulta";

			this.$resultsTopbarMessageElement.html(message);
		}
	});

	/**
		UserMessengerView

		Correspone a una vista que informa al usuario de estados de Medic y otros.
	**/

	var UserMessengerView = Backbone.View.extend({
		el: '#user-messenger',

		initialize: function(){
			this.$grandMessageContainer = this.$el.find('#grand-message-container');
			this.$tinyMessageContainer = this.$el.find('#tiny-message-container');

			this.showConsultingMessageCount = 0;
		},

		clearMessages: function(){
			this.$tinyMessageContainer.html("");
			this.$grandMessageContainer.html("");
		},

		showResultsMessage: function(resultsCount){
			this.clearMessages();
			var message  = "";

			if (resultsCount > 0)
				if (resultsCount == 1)
					message = "Hemos encontrado un resultado para su consulta"
				else
					message = "Se encontraron <strong>" + resultsCount + " resultados</strong> para su consulta";
			else
				message = "No se encontraron resultados para su consulta";

			this.$tinyMessageContainer.html(message);	
		},

		showConsultingMessage: function(){
			this.clearMessages();

			var message = "";

			if (!this.showConsultingMessageCount % 2)
				message = "Un momento... Analizamos <strong>" + Medic.totalResults +"</strong> posibles resultados";
			else
				message = "Otro momento... Analizamos <strong>" + Medic.totalResults +"</strong> posibles resultados";

			this.$grandMessageContainer.html(message);
			this.showConsultingMessageCount++;
		}

	});

	var UserMessenger = new UserMessengerView();

	var Medic = new MedicView();

	var EntityCardView = Backbone.View.extend({
		
		tagName: 'a',
		className: 'medic_card result-item',		

		initialize: function(){

			var card_selector = "";			
			var n_header = 1;

			if ( this.model.isDoctor() ){
				this.$el.addClass("doctor_card");
				card_selector = "doctor";
				n_header = Math.floor((Math.random() * Medic.max_doctor_headers) + 1);				
			}else{				
				this.$el.addClass("institution_card");
				card_selector = "institution";			
				n_header = Math.floor((Math.random() * 3) + 1);				
			}

			this.model.set('header_img', n_header);

			if (this.model.isJoined() ){
				this.$el.addClass("joined");
				card_selector = card_selector + "_joined";
			}

			this.template = Medic.templates.card_views[card_selector];

		},

		render: function(){			

			this.prepareModelToView();
			
			var renderized  = Mustache.render(this.template, this.model.attributes);			
	
			this.$el.html(renderized);						
			this.$el.addClass("hide");

			this.$el.attr("href", "/" + this.model.get('usuario'));
			
			return this;			
		},

		prepareModelToView: function(){
			var entity      = this.model.attributes;			
			entity.url_foto = Medic.media_url + entity.foto;

			if (this.model.isDoctor()){
				// Preapación de especialidades

				var n_especialidades = entity.especialidades.length;				

				for ( i = 1; i < n_especialidades; i++)
					entity.especialidades[i].nombre_especialista = " - " + entity.especialidades[i].nombre_especialista;

				// Preparación del título

				if (entity.sexo == Medic.consts.SEXOS.FEMENINO)
					if (entity.titulo == Medic.consts.TITULOS.DOCTOR)
						entity.titulo = 'Dra';	
			}

		}

	});

	// Contenedor de resultados

	var ResultsContainerView = SocketListenerView.extend({

		el: "#resultados",
		socket: Medic.mse_socket,		
		entities: EntitiesResult,

		events: {
			"webkitAnimationEnd .medic_card" : "nextEntityShow",
			"oanimationend .medic_card" : "nextEntityShow",
			"msAnimationEnd .medic_card" : "nextEntityShow",
			"animationend .medic_card" : "nextEntityShow",
		},

		socket_events: {
			'incoming': 'prepareResults'
		},

		initialize: function(){
			this.__initialize();

			this.entities.on("reset", this.render, this);
			this.notDelay = false;

			this.$noResultsMessage = $("#no-results-message");
			this.$noEnoughInfo = $("#no-enough-info");
		},

		render: function(){
			/* Reponde al evento reset de las entidades */


			var results_view = this;

			Medic.prepareForShowResults();
			UserMessenger.showResultsMessage(this.entities.length);				

			if (this.entities.length > 0){

				this.entities.forEach(function(ent){
					var card_view = new EntityCardView({model: ent});
					results_view.$el.append(card_view.render().el);				
					
				});

				this.showResults();
			}else{

				if (this.mso.response_code == Medic.MSEResponses.NOT_RELEVANT_INFO){
					this.$noEnoughInfo.removeClass("hide");
				}else{
					this.$noResultsMessage.removeClass("hide");	
				}				
			}

			this.$el.removeClass("hide");

			return this;

		},

		nextEntityShow: function(e){
			var e = $($(".medic_card.hide")[0]);    	
	    	e.removeClass("hide");
	    	e.addClass("show-doctor");

	    	var servicios_container = e.find(".servicios")[0];
	    	
		},

		prepareResults: function(mso){
			//console.log("Se recibió del MSE");
			//console.log(mso);

			this.mso = mso;			

			if (mso.response_code == Medic.MSEResponses.RESULTS_RESTORED){
				this.notDelay = true;
			}else
				this.notDelay = false;

			var results = mso.results;
			var models = [];

			_.each(results, function(ent){
				entity_model = new EntityModel(ent);				
				models.push(entity_model);
			});

			this.entities.reset(models);
		},

		showResults : function(){			

			// Incialización de Masonry
			this.msnry = new Masonry(this.$el.selector, {
        		itemSelector: '.medic_card',
    		});     						    	
								
			
			if(this.notDelay)
				this.showFast();
			else
				this.showSlowly();
		},

		showSlowly: function(){
			$(".medic_card:first-child").removeClass("hide");
			$(".medic_card:first-child").addClass("show-doctor");  	
		},

		showFast: function(){
			$(".medic_card").removeClass("hide");
			$(".medic_card").addClass("show-doctor");

		},

		clearResults: function(){

			this.$el.html("");
			this.$noResultsMessage.addClass("hide");
			this.$noEnoughInfo.addClass("hide");

			if (this.msnry )
				this.msnry.destroy();
		}

	});	

	var BuscardorInputView = Backbone.View.extend({

		el: "#busqueda",

		events: {
			"keyup #consulta" : "startSearch",
			"click #boton-buscar" : "startSearch"			
		},		

		fixedSearchBarConditions: function(){

			var scroll_position_top = $(document).scrollTop();

	        if (this.search_bar_pos < scroll_position_top ){

	        	if (!this.searchBarFixed){
	        		this.$heightSaver.height(this.search_bar_height);
	            	this.$el.addClass('fixed');            	
	            	this.searchBarFixed = !this.searchBarFixed;
	        	}
	            
	        }else{

	        	if (this.searchBarFixed){
	        		this.$heightSaver.height(0);
	            	this.$el.removeClass('fixed');	
	            	this.searchBarFixed = !this.searchBarFixed;
	        	}
	        	
	        }
	        		    
		},		

		makeMSO: function(plain_query){

			var mso = {};
			mso.plain_query = plain_query;
			mso.user_session = $.cookie("sessionid");			

			$.cookie("last_query", plain_query);

			return mso;

		},

		startSearch: function(event){			

			if (event.type == "keyup")
				if (event.keyCode != 13)
					return;		

			if (Medic.mobile)
				this.input.blur();


			var plain_query = this.$("input").val();	

			if (plain_query != ''){
				this.resultsView.clearResults();					
				UserMessenger.showConsultingMessage();
				Medic.preSearch();
				
				mso = this.makeMSO(plain_query);

				Medic.mse_socket.emit('search', mso); 			
				// TODO: Si es plana debería notificar que debe introducir una búsqueda
			}

		},

		initialize: function(options){

			this.input = document.getElementById("consulta");

			this.searchBarFixed = false;
			this.$heightSaver = $("#busqueda_height_saver");

			this.resultsView = options.resultsView;

			this.search_bar_pos = this.$el[0].offsetTop;
    		this.search_bar_height = this.$el[0].offsetHeight;    

    		_.bindAll(this, 'fixedSearchBarConditions');  		

    		$(document).on('scroll', this.fixedSearchBarConditions);    	

    		if (!Medic.mobile) 
    			this.input.focus();    		

    		if (this.input.value != ""){
    			var mso = this.makeMSO(this.input.value);
    			Medic.preRecovery();		
    			Medic.mse_socket.emit('recover_search', mso);
    		}
		},

	});	

	var ResultsContainer = new ResultsContainerView();	
	var BuscardorInput = new BuscardorInputView({resultsView: ResultsContainer} );	
});