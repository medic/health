$(function(){

	var ANIMATION_CLASS = "reveal-left-right";

	var title = $("#title")[0]

	var titleEnds = title.offsetTop;
	
	var isTitleHidden = false;

	var barPageTitle = $("#page-title");
	var barSlogan = $("#slogan");


	/** 
		Si se detecta que el usuario no viene de realizar una búsqueda
		se elimna el botón para voler.
	**/
	if ($.cookie("last_query") == undefined){
		$(".back-button").remove();
	}

	function changeBarTitle(){

		var scrollPositionTop = $(window).scrollTop();

		if ( scrollPositionTop > titleEnds){
			if (!isTitleHidden){
				barPageTitle.addClass(ANIMATION_CLASS);
				barPageTitle.removeClass("hidden")
				barPageTitle.show();				
				barSlogan.hide();
				barSlogan.removeClass(ANIMATION_CLASS);				
				isTitleHidden = !isTitleHidden;				
			}			
		}else{
			if (isTitleHidden){				
				barPageTitle.hide();
				barPageTitle.removeClass(ANIMATION_CLASS);				
				barSlogan.addClass(ANIMATION_CLASS);
				barSlogan.show();				
				isTitleHidden = !isTitleHidden;				
			}
		}
			
		
		
	}

	changeBarTitle();
	$(document).on('scroll', changeBarTitle);

	$(".back").on('click', function(evt){
		evt.preventDefault();
		window.history.back();
	})

});