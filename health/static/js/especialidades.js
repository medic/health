function limpiar_formulario(selector_contenedor){

	$(selector_contenedor + " input ").val("");
	$(selector_contenedor + " textarea ").val("");
}
// Modelo para una especialidad

var Especialidad = Backbone.Model.extend({	

	idAttribute: 'id',
	urlRoot: '/datamark/api/especialidad/',

	defaults: {
		'id': null,
		'especialidad': '',
		'descripcion': '',
		'nombre_especialista': '',
		'palabras_claves': '',
		'n_especialistas': 0
	}

});

// Colección de especialidades

var Especialidades = Backbone.Collection.extend({

	url: 'todos',
	model: Especialidad
});

// Vista para el formulario de una especialidad

var FormularioEspecialidadVista = Backbone.View.extend({

	id: "especialidad_form",

	initialize: function(){

		_.bindAll(this, 'registrar_especialidad');

		$("#" + this.id + " .registro-submit").click(this.registrar_especialidad);
	},

	mostrar: function (modelo) {

		if (modelo){

			this.model = modelo;
			var campos = _.keys(modelo.defaults);
		
			_.each(campos, function(campo_nombre){

				$("#id_" + campo_nombre).val(modelo.get(campo_nombre));
			});
		}else{
			this.model = new Especialidad();
		}		
			
		$('#especialidad_form').modal({show:true});

		
	},

	registrar_especialidad: function(event){

		console.log(this.model);
		

		var especialidad = $("#id_especialidad").val();
		var nombre_especialista = $("#id_nombre_especialista").val();
		var palabras_claves = $("#id_palabras_claves").val();
		var descripcion = $("#id_descripcion").val();
		var id = $("#id_id").val();

		$("#especialidad_form").modal('hide');

		var modelo = this.model;

		var campos = _.keys(this.model.defaults)
		_.each(campos,function(campo){
			modelo.set(campo, $("#id_"+campo).val());
		});

		if (id == 0)
			window.especialidades_vista.agregar_especialidad(especialidad, nombre_especialista, palabras_claves, descripcion);
		else{
			console.log("Entro en este else");
			window.especialidades_vista.actualizar_especialidad(this.model);
		}
			
		limpiar_formulario("#especialidad_form");
		$("#especialidad_form #id_id").val(0);

	}


});

// Vista para una especialidad

var EspecialidadVista = Backbone.View.extend({

	tagName: "tr",
	className: "especialidad_row",
	initialize: function(){								

		texto = this.render();		

		this.model.on("change", function(){
			console.log("Se llamó al render");
			this.render();
		}, this);
	},

	events:{
		"click .editar" : "formulario_editar"
	},

	render: function(){

		especialista = "<td>" + this.model.get('nombre_especialista')+ "</td>";
		nombre_scp = "<td>" + this.model.get('especialidad')+ "</td>";

		cuenta_scp = "<td>" + this.model.get('n_especialistas') + "</td>";
		palabras_scp = "<td>" + this.model.get('palabras_claves') + "</td>";		
		botones_scp = ' <td> ' +						
							'<div class="btn-group" >' +
								'<span class="btn consulta" >' +
									'<i class="icon-eye-open"></i>' +
								'</span>	' +
								'<span class="btn editar" href="">' +
									'<i class="icon-pencil"></i>'+
								'</span>'+
								'<a class="btn eliminar">'+
									'<i class="icon-remove"></i>'+
								'</a>'+
							'</div>' +				
						'</td>';

		this.$el.html(especialista + nombre_scp + palabras_scp + cuenta_scp  + botones_scp);
		return this.$el;
				
	},

	formulario_editar: function(event){
		window.form.mostrar(this.model);
	}

});

var EspecialidadesVista = Backbone.View.extend({

	tagName: 'tbody',
	id: 'especialidades',

	initialize: function(){		

		_.bindAll(this, 'agregar_especialidad', 'mostrar_especialidad_agregada');

		data = {};
		this.especialidades = new Especialidades();			
		data = this.options['data_inicial'];
		this.especialidades.reset(data);				
		this.especialidades.on('add', this.mostrar_especialidad_agregada);
				
		var parent = this.$el;

		_.each(this.especialidades.toArray(), function(especialidad){								
			
			var vista = new EspecialidadVista({model: especialidad});
			parent.append(vista.$el);
			
		});

		$('table.table').append(this.$el);
	},

	agregar_especialidad: function(especialidad, nombre_especialista, palabras_claves, descripcion){

		if (especialidad != "" ){
			s = new Especialidad({								
				'especialidad': especialidad,
				'nombre_especialista': nombre_especialista,
				'palabras_claves': palabras_claves,
				'descripcion': descripcion,
				'n_especialistas': 0				
			});			

			s.save()

			this.especialidades.add(s);
			
		}else
			mostrar_aviso("Los campos se encuentra vacios");
		
	},

	mostrar_especialidad_agregada: function(){				

		nueva_especialidad = this.especialidades.last();		

		var vista = new EspecialidadVista({model: nueva_especialidad});		

		console.log(vista.$el);

		$(this.$el).append(vista.$el); 
	},

	actualizar_especialidad: function(modelo){
		modelo.save();
	}


});

jQuery(document).ready(function($) {

	var inicial = $("div.data_especialidades").data("inicial");		
	//$("div.data_especialidades").remove();

	var h_esp = new EspecialidadesVista({'data_inicial': inicial});
	

	window.form = new FormularioEspecialidadVista();

	window.especialidades_vista = h_esp;

	var _sync = Backbone.sync;

	Backbone.sync = function(method, model, options){

		options.beforeSend = function(xhr) {
			var token = $("input[name=csrfmiddlewaretoken]").val();
			xhr.setRequestHeader('X-CSRFToken', token);
		};

		options.error = function(jqXHR, textStatus, errorThrown){
            
            console.log(jqXHR.responseText);
        }

        options.complete = function(jqXHR, textStatus, errorThrown){            
        	
    		if (method == "create"){
        		var respuesta = jqXHR.getAllResponseHeaders();              	
	        	var url_location = respuesta.split("\n")[1].split("/");
	        	var id = url_location[url_location.length -2];	
	        	model.set('id', id);
	        }
        	        	                               
        }		

		return _sync(method, model, options);
	};

	$("#form_toggle").click(function(){
		window.form.mostrar();
	})
	$('#example').dataTable( {
		    	"sPaginationType": "full_numbers",
		        "oLanguage": {
		            "sLengthMenu": "Mostrar _MENU_ registros",
		            "sZeroRecords": "No existen registros.",
		            "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
		            "sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
		            "sInfoFiltered": "(filtered from _MAX_ total records)",
		            "sSearch": "Filtrar: ",
		            "oPaginate": {
		            	"sPrevious": "Anterior",
				        "sNext": "Siguiente"
				      }		            		         
		        }
	} );
	

});
