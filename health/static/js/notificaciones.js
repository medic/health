
// Funciones para el uso de notificaciones 

// Especifica si una notificación se está mostrando
var notificacion_abierta = false;

// Esconde la notificación

function esconder_notificacion(){

	if (notificacion_abierta){

		notificacion_abierta = false;

		$("#notificacion").animate({
			bottom: '-120'
			}, 800, function(){

			}
		);			
	}

}

/* Muestra una notificación con el mensaje especificado.
*  @param mensaje Cadena con el mensaje.
*/

function mostrar_notificacion(mensaje){

	if (!notificacion_abierta){

		notificacion_abierta = true;

		$("#notificacion p").html(mensaje);

		$("#notificacion").animate({
			bottom: '+0'
		}, 700, function(){
			setTimeout(esconder_notificacion, 5000)
		});
	}

	// Sería bueno implementar una pila de notificaciones, digo yo.
	
}

// Notificación con cabecera naranja para advertencias

function mostrar_aviso(mensaje){

	$("#notificacion .cabecera").css("background-color", "orange");
	mostrar_notificacion(mensaje);
}

// Notificacíón con cabecera roja para errores.

function mostrar_error(mensaje){

	$("#notificacion .cabecera").css("background-color", "#DD0808");
	mostrar_notificacion(mensaje);
}

// Notificación con cabecera verde para mostrar confirmaciones.

function mostrar_confirmacion(mensaje){
	$("#notificacion .cabecera").css("background-color", "green");
	mostrar_notificacion(mensaje);
}