var form;

/*
        Agrega un nuevo formulario a un formset.
        -target: prefix del formset.
        -selector_contenedor: selector css al contenedor donde se encuentras los formularios.
        -form_html: HTML del formulario en bruto (sin prefix definido).
*/


function agregar_formset(target, selector_contenedor, form_html, agregar){

        var total_forms = $("#id_"+target+"-TOTAL_FORMS");
        var n_forms = total_forms.val();

        form = form_html;

        var form_preparado = form_html.replace(/__prefix__/g, n_forms);        

        n_forms++;
        total_forms.val(n_forms);

        $(selector_contenedor).append(form_preparado);
}

/*
        Agrega un nuevo formulario a un formset mediante antes del elemento especificado en antecesor.
        -target: prefix del formset.
        -selector_contenedor: selector css al contenedor donde se encuentras los formularios.
        -form_html: HTML del formulario en bruto (sin prefix definido).
        -antecesor: selector del elemento que marca el elemento sucesor del formulario a agregar.
*/


function agregar_formset_antes(target, selector_contenedor, form_html, selector_antecesor){

        var total_forms = $("#id_"+target+"-TOTAL_FORMS");
        var n_forms = total_forms.val();

        form = form_html;

        var form_preparado = form_html.replace(/__prefix__/g, n_forms);        

        n_forms++;
        total_forms.val(n_forms);

        $(selector_antecesor).before(form_preparado);

        return n_forms-1;

}

function eliminar_formset(target, selector_contenedor, clase_form, posicion){

        form = $(selector_contenedor + " " + "." + clase_form +":eq("+posicion+")");
        console.log(form);

        dec_total_forms(target);
        alert(form.html())
        form.remove();
}


/*
        Obteiene el numero actual de formularios de un formset.
        -taget: prefix del formset.
        return Total de formularios del formset.
*/

function obtener_total_forms(target){

        return parseInt($("#id_"+target+"-TOTAL_FORMS").val());
}

/*
        Obtiene el numero máximo de formularios del formset.
        -taget: prefix del formset.
        return Número máximo de formularios del formset.
*/

function obtener_max_num_forms(target){
        return $("#id_"+target+"-MAX_NUM_FORMS").val(); 
}

/*
        Incrementa el número de formularios de un formset
        -target: prefix del formset.    
*/

function inc_total_forms(target){

        var total_forms = $("#id_"+target+"-TOTAL_FORMS");
        var n_forms = total_forms.val();
        n_forms++;
        total_forms.val(n_forms);

}

/*
        Decrementa el número de formularios de un formset.
        -target: prefix del formset.
*/

function dec_total_forms(target){       

        var total_forms = $("#id_"+target+"-TOTAL_FORMS");
        var n_forms = total_forms.val();

        console.log('su valor es ' + n_forms);

        n_forms--;
        total_forms.val(n_forms);

}


/*
        Verifica si el formulario con el selector indicado esta vacio.
        -selector: contenedor del formulario a verificar.
        -inputs_text: arreglo con los fragmentos de id de los text imputs a verificar.
        -inputs_area: arreglo con los fragmentos de id de los text area a verificar.
*/

function is_form_empty(selector,inputs_text,inputs_area){
        sw = true
        sw1 = false
        
        if(inputs_text.length>0){
                console.log("aqui voy")
                 
                $.each(inputs_text, function (ind, elem) {
                        $( selector +" input[id*="+elem+"]").each(function (index) {
                        
                                elemento = $(this) //$( selector + " input[id*="+elem+"]")
                                console.log(selector + " input[id*=elem]")
                                if(elemento.attr('id')!='filtro' && elemento.attr('id').search('__prefix__') == -1 ) //Inputs que no se deben verificar
                                        if(elemento.val() == ''){
                                                console.log("esta vacio = True");
                                                sw = false
                                                return false
                                        }
                        })
                });     
        }else{
    
        console.log('Lista de inputs_text vacia!')                
                $( selector + " input[type=text]").each(function (index) {

                if($(this).attr('id')!='filtro' && $(this).attr('id').search('__prefix__') == -1 ) //Inputs que no se deben verificar
                       console.log("revisando si hay uno lleno")
                    console.log($(this).val())
                        if($(this).val() != ''){
                                console.log("esta lleno = false");
                                sw = false
                                return false
                        }            
                }) 
                        
        }
        if(inputs_area.length>0){
                $.each(inputs_area, function (ind, elem) {
                        console.log('¡Hola :'+elem+'!');
                         $( selector +" textarea[id*="+elem+"]").each(function (index) {
                                elemento = $(this) // $( selector + " textarea[id*="+elem+"]")
                                //console.log(selector + " input[id*=elem]")
                                if(elemento.attr('id')!='filtro' && elemento.attr('id').search('__prefix__') == -1 ) //Inputs que no se deben verificar
                                        if(elemento.val() == ''){
                                                console.log("Esta Vacio "+elemento.attr('id'));
                                                sw = false
                                                return false
                                        }
                        })
                }); 
        }else{
            

                console.log('Lista de inputs_area vacia!')  
                $( selector + " textarea").each(function (index) {
                    
                        if($(this).val() != ''){
                                console.log("Esta lleno "+$(this).attr('id'));
                                sw = false
                                return false
                        }            
                }) 
                  
        }

return sw
}

function delete_form(){
    $('.close').live('click',function(){
            console.log("Procedimiento de Eliminar: ")
            $('#' + $(this).attr('id')+" input[type='checkbox']" ).prop('checked', true);
            if(!$('#' + $(this).attr('id')+" input[type='checkbox']" ).attr('id')){
                    $(this).attr('data-dismiss','alert')
                    $(this).click()
                    console.log("Se elimina")
            }else{
                    console.log($(this).parent().prev().html())//
                    $(this).parent().addClass('invisible')
                    console.log("Se conserva y se esconde")
            }
    });
}