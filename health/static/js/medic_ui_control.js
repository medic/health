
var mobile = false;


// Cambios en la interfaz para mostrar resultados
function mostrar_resultados(){    

    if (mobile){
        $("#resultados").addClass("mobile");
        $("#cuerpo").addClass("mobile");
    }

	$("#presentacion").addClass("hide");
	$("#barra-superior h3").addClass("hide");
	$("#filtros").removeClass("hide");
	$("#filtros").addClass("show");
	$("#resultados").removeClass("hide");	
	$("article.medic_card:first-child").removeClass("hide");
	$("article.medic_card:first-child").addClass("show-doctor");    

}

$('document').ready(function(){
    //var parentHeight = $('#consulta').height();
    //$('#boton-buscar').height(parentHeight);

    mobile = (window.innerWidth <= 768)? true : false;
    document.getElementById("consulta").focus();

    // Inicialización de Masonry

    var container = document.querySelector("#resultados");
    var msnry = new Masonry(container, {
        itemSelector: '.medic_card',

    });


    $("#boton-buscar").click(function(){
 		mostrar_resultados();   	
    });

    $("input#consulta").keyup(function(event){
        if (event.keyCode == 13)
            mostrar_resultados();
    });

    $("article.medic_card").on("webkitAnimationEnd oanimationend msAnimationEnd animationend", function(event){    	
    	var e = $($("article.medic_card.hide")[0]);    	
    	e.removeClass("hide");
    	e.addClass("show-doctor");
    });

    // Control de la barra de búsqueda para situarse en la parte superior al hacer
    // scroll

    var search_bar_pos = $("#busqueda")[0].offsetTop;
    var search_bar_height = $("#busqueda")[0].offsetHeight;    

    $(document).on('scroll', function(){
        var scroll_position_top = $(document).scrollTop();

        if (search_bar_pos < scroll_position_top ){
            $("#busqueda_height_saver").height(search_bar_height);
            $("#busqueda").addClass('fixed');            
        }

        if (search_bar_pos > scroll_position_top ){
            $("#busqueda_height_saver").height(0);
            $("#busqueda").removeClass('fixed');
        }
    });


});