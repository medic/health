/* 
	Funcionalidad javascript usado en formulario de medio publicitario
*/


function formulario_vacio(){

	entradas = $(".tarjeta-telefono input[type=text][id*=telefono]")

	n = entradas.length-1; // Se decrementa uno adicional para que no tome el formulario modelo de un servicio

	for ( i = 0; i < n; i++)
		if (entradas[i].value == "" ){			
			return true;
		}
			

	return false;
}


function toggle_zona(){
	if ($("#id_ciudad")[0].value == ""){
		$("#id_zona")[0].disabled = true;
		$("#id_zona")[0].value = "";
	}else
		$("#id_zona")[0].disabled = false;
}
/*
	Desactiva una opcion de un select.
	Es una función para el objeto JQuery. Recibe un boolean que especifica si se mostrará o no.
	Una llamada puede ser $(<selector>).conmutarOpcion(false)
*/

jQuery.fn.conmutarOpcion= function( show ) {
    jQuery( this ).toggle( show );
    if( show ) {
        if( jQuery( this ).parent( 'span.toggleOption' ).length )
            jQuery( this ).unwrap( );
    } else {
    	if( !jQuery( this ).parent( 'span.toggleOption' ).length )
        	jQuery( this ).wrap( '<span class="toggleOption" style="display: none;" />' );
    }

}

function filtrar_aseguradora(consulta){
	opciones = $("select#id_seguros option");
	consuta = normalizar_cadena(consulta);
	var je;

	opciones.each(function(i,e){
	je =$(e);
	if (normalizar_cadena(je.html()).indexOf(consulta)== -1){
		$(e).conmutarOpcion(false);
	}else
		$(e).conmutarOpcion(true);
	});

}


// Javascript al cargarse el documento

jQuery(document).ready(function($) {
	
	$("#filtro_seguros").keyup(function(event){

		filtrar_aseguradora(event.currentTarget.value);
	});

	target = 'telefonos'
	
	toggle_zona();
	

	$("#id_ciudad").on("change", toggle_zona);
	
	$("#telefonos-forms .tarjeta-agregar").on("click", function(event){		
		
		if (!formulario_vacio()){
			html_servicio = $("#telefono-model-form").html();

			console.log(html_servicio);
			agregar_formset_antes(target, "#telefonos-forms", html_servicio, "#telefonos-forms .tarjeta-agregar");
		}else	
			mostrar_aviso("Debes usar los formularios que aún están vacios para agregar uno nuevo");
	});

	$("#horarios-forms .tarjeta-agregar").on("click", function(event){		
				
		//if (!formulario_vacio()){
			html_servicio = $("#horario-model-form").html();
			agregar_formset_antes('horarios', "#horarios-forms", html_servicio, "#horarios-forms .tarjeta-agregar");
			todo_momento_input = document.getElementById('id_todo_momento');
			todo_momento_input.disabled = true;
			elemento = $(this).prev().attr('id')
			
			$("#"+elemento + " input[name*='-manana_hora_inicio']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackManana, showPeriod: true,
		    showLeadingZero: true});

		    $("#"+elemento + " input[name*='-manana_hora_fin']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackMananaFin, showPeriod: true,onMinuteShow: OnMinuteShowCallbackManana,
		    showLeadingZero: true});
		    elemento = $(this).prev().attr('id')
		    console.log("aqui esta para la tarde" + elemento)
			 $("#"+elemento + " input[name*='-tarde_hora_inicio']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackTarde, showPeriod: true,
		    showLeadingZero: true});
		     $("#"+elemento + " input[name*='-tarde_hora_fin']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackTardeFin, showPeriod: true,
		    showLeadingZero: true});
		    });
		//}else	
			//mostrar_aviso("Debes usar los formularios que aún están vacios para agregar uno nuevo");

			delete_form()
	
			
	});

	
	


	$('input#id_todo_momento').change(function(event){

		console.log(event.currentTarget);

		if ( event.currentTarget.checked)
			$('#horarios-forms').hide();
		else
			$('#horarios-forms').show();
	});

	



 	function OnHourShowCallbackMananaFin(hour) {
    	inicio = $(this).parent().parent().prev().children('div').children('input').attr('id')
		    	
	    if ((hour <  $("#"+inicio).timepicker('getHour') + 1) || (hour > 12)  ) {
	        return false; // not valid
	    }
	    return true; // valid
	}

	function OnHourShowCallbackManana(hour) {
	    if ((hour > 12) || (hour < 6)) {
	        return false; // not valid
	    }
	    return true; // valid
	}
	function OnHourShowCallbackTarde(hour) {
	    if ((hour < 12) || (hour < 6)) {
	        return false; // not valid
	    }
	    return true; // valid
	}
	function OnHourShowCallbackTardeFin(hour) {
		inicio = $(this).parent().parent().prev().children('div').children('input').attr('id')
		    	

	    if ((hour <  $("#"+inicio).timepicker('getHour') + 1)) {
	        return false; // not valid
	    }
	    return true; // valid
	}
	function OnMinuteShowCallbackManana(hour, minute) {
    if ((hour == 12) && (minute > 0)){ return false; } // not valid
    return true;  // valid
}