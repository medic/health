
function desactivar_check(check){

	var label = $(check.parentNode);
	label.removeAttr("style");

}

function activar_check(check){

	var label = $(check.parentNode);
	label.attr("style", "color: #000; font-weight: bolder");

	console.log("Debió activa");

	
}


function cambiar_habilitacion(check){
	
	var label = $(check.parentNode);	

	if (check.disabled){
		label.removeClass("seleccionado");
		label.attr("class", "inhabilitado");
	}else
		label.removeClass("inhabilitado");
}

function cambiar_check(check){

	var label = $(check.parentNode);	

	if (check.checked){
		label.addClass("seleccionado");		
		label.addClass("entypo-check");	
	}else{
		label.removeClass("seleccionado");		
		label.removeClass("entypo-check");	
	}
}

function como_opcion_unica(event){
	
	// Input seleccionado como objeto JQuery
	var check = $(event.currentTarget)[0];

	// Texto con el que inician los IDs de todos los inputs hermanos del input seleccionado
	var bloque_horario_id = check.id.substr(0,check.id.length-2);  

	var activado = check.checked

	var todos_dias = false;

	if (check == $("div.dias_input li:last-child input")[0] )	
		todos_dias = true;
	
	var checks = $("input[type=checkbox][id^=" + bloque_horario_id + "]");

	for (var i = checks.length - 1; i >= 0; i--) {
		if (activado){
			checks[i].checked = !activado;
			cambiar_check(checks[i]);
			
		}
			
		if ( i < 5 + 2*todos_dias){
			checks[i].disabled = activado;
		}else{
			if (!todos_dias && checks[i].disabled)
				checks[i].disabled = false;
		}

		cambiar_habilitacion(checks[i]);
	};		

	if (activado){
		check.disabled = !activado;
		cambiar_habilitacion(check);
	}

	check.checked = activado;		
	cambiar_check(check);
	

}

function resaltar_seleccion(event){	

	var check = $(event.currentTarget)[0];		

	cambiar_check(check);

}

$(document).ready(function(){	

	$("input[type=checkbox][name$=dias][value=8]").live("change",como_opcion_unica);
	$("input[type=checkbox][name$=dias][value=9]").live("change",como_opcion_unica);

	$("input[type=checkbox][name$=dias]").live("change",resaltar_seleccion);

	$("input[type=checkbox][name$=dias]").each(function (i, e){
		cambiar_check(e);
	});
});