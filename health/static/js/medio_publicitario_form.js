
/* 
	Funcionalidad javascript usado en formulario de medio publicitario
*/

function formulario_vacio(){

	entradas = $(".tarjeta-servicio input[type=text][id*=descripcion]")

	n = entradas.length-1; // Se decrementa uno adicional para que no tome el formulario modelo de un servicio

	for ( i = 0; i < n; i++)
		if (entradas[i].value == "" ){
			console.log("En la posicion "+i);
			return true;
		}
			

	return false;
}

// Javascript al cargarse el documento

jQuery(document).ready(function($) {	

	target_servicio = 'servicio_publicitario_set'
	
	$(".tarjeta-agregar").on("click", function(event){		
		
		if (!formulario_vacio()){
			html_servicio = $("#servicio-model-form").html();
			agregar_formset_antes(target_servicio, "#servicios-forms", html_servicio, ".tarjeta-agregar");
		}else	
			mostrar_aviso("Debes usar los formularios que aún están vacios para agregar uno nuevo");
	});

	$("servicios-forms .close").live("click", function(event){
		
		index = $(event.currentTarget).index(".close");		
		eliminar_formset(target_servicio, "#servicios-forms", "tarjeta-servicio", index);

	});
});