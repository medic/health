/*
	Javascript para el formulario de doctores
*/

/*
	Desactiva una opcion de un select.
	Es una función para el objeto JQuery. Recibe un boolean que especifica si se mostrará o no.
	Una llamada puede ser $(<selector>).conmutarOpcion(false)
*/

jQuery.fn.conmutarOpcion= function( show ) {
    jQuery( this ).toggle( show );
    if( show ) {
        if( jQuery( this ).parent( 'span.toggleOption' ).length )
            jQuery( this ).unwrap( );
    } else {
    	if( !jQuery( this ).parent( 'span.toggleOption' ).length )
        	jQuery( this ).wrap( '<span class="toggleOption" style="display: none;" />' );
    }

}

function verificar_usuario(usuario){	

	var control = $("#control_usuario"); 
	var info = $("#control_usuario .controls span");

	if ( usuario.length > 6 ){

		$.get('disponible', {'usuario_doctor': usuario }).done(function(data){
				
			if (data == "1"){
				control.removeClass("error");
				control.addClass("success");								
				info.html("");
				info.html("¡Disponible!");
			}else{
				control.addClass("error");
				control.removeClass("success");
				info.html("");
				info.html("No disponible");
				
			}				
		});
	}else{
		$("#control_usuario").removeClass("error");
		$("#control_usuario").removeClass("success");
		info.html("");
	}
}

/*
	Genera un nombre de usuario a partir del nombre y el apellido en el formulario
*/

function usuario_nombre_apellido(event){


		apellidos = $("#id_apellido").val();
		nombres = $("#id_nombre").val();

		primer_apellido = apellidos.split(" ")[0].toLowerCase();
		primer_nombre = nombres.split(" ")[0].toLowerCase();

		var usuario_gen = normalizar_cadena(primer_nombre + primer_apellido);

		$("#id_usuario").val(usuario_gen);

		verificar_usuario(usuario_gen);

		
}

function capitalizar_valor (event) {

	input = $(event.currentTarget);

	valor = input.val();

	if ( valor != ""){
		cadenas = valor.split(" ")

		for (var i = cadenas.length - 1; i >= 0; i--) {
			cadenas[i] = cadenas[i][0].toUpperCase() + cadenas[i].slice(1);
		}	

		var nuevo_valor = ""

		n = cadenas.length
		for (var i = 0; i < n; i++) {
			nuevo_valor = nuevo_valor + " " + cadenas[i]
		}

		input.val(nuevo_valor.slice(1));
	}

}


/*
	Realiza el filtrado de especialidades a partir de la cadena 
	especificada en el filtro
*/

function filtrar_especialidades(consulta){

	opciones = $("select#id_especialidades option");

	consulta = normalizar_cadena(consulta);

	var je;

	opciones.each(function(i, e){

		je = $(e);

		if ( normalizar_cadena(je.html()).indexOf(consulta) == -1 ){			
			$(e).conmutarOpcion(false);
		}else
			$(e).conmutarOpcion(true);
	});
}

var c;

function filtrar_aseguradora(consulta){
	opciones = $("select#id_seguros option");
	consuta = normalizar_cadena(consulta);
	var je;

	opciones.each(function(i,e){
		je =$(e);
		if (normalizar_cadena(je.html()).indexOf(consulta)== -1){
			$(e).conmutarOpcion(false);
		}else
			$(e).conmutarOpcion(true);
	});

}

$(document).ready(function(){	


	delete_form()
	// Generación y validación de usuario

	$("#id_apellido").keyup(usuario_nombre_apellido);
	$("#id_nombre").keyup(usuario_nombre_apellido);
	$('#id_nombre').focusout(capitalizar_valor);
	$('#id_apellido').focusout(capitalizar_valor);
	$('#id_usuario').keyup(function(event){
		verificar_usuario(event.currentTarget.value);
	});

	// Filtro de especialidades

	$("#filtro_especialidades").keyup(function(event){
		filtrar_especialidades(event.currentTarget.value);
	});	

	$("#filtro_seguros").keyup(function(event){
		filtrar_aseguradora(event.currentTarget.value);
	});

	/* Gestión de formsets para téléfonos */

	$("#telefonos-forms .tarjeta-agregar").click(function(event){

		window.evento = event;
		console.log(is_form_empty("#telefonos-forms",[],[]))
		if(is_form_empty("#telefonos-forms",[],[])==true){
			
			mostrar_aviso("Debes usar los formularios que aún están vacios para agregar uno nuevo");
		}else{
			html = $("#telefono-model-form").html();
			agregar_formset_antes("telefonos", ".telefonos-forms", html, "#telefonos-forms .tarjeta-agregar");
		}
					
	});

	/* Gestión de formsets de CONSULTAS */
	target = 'consulta_set'
	
	$(".tarjeta-agregar-consulta").on("click", function(event){		
		array1 = []
		array2 = ['detalle_sitio']
		if(is_form_empty(".citas",array1,array2)){
			mostrar_aviso("Por favor, verifica los campos de <b> Extencion de telefono, Detalles de sitio</b> , puede que aún están vacios.");
		}else{
			html = $("#consulta-model-form").html();
			
			agregar_formset_antes(target, ".citas", html, ".tarjeta-agregar-consulta");			
			padre = $(this).prev().children('input').parent().attr('id')
			
			$("#"+padre+" input[name*='manana_hora_inicio']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackMananaLive, showPeriod: true,
    showLeadingZero: true});
				
			 $("#"+padre+" input[name*='manana_hora_fin']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackMananaFinLive, showPeriod: true,onMinuteShow: OnMinuteShowCallbackManana,
    showLeadingZero: true});



			$("#"+padre+" input[name*='tarde_hora_inicio']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackTardeLive, showPeriod: true,
		    showLeadingZero: true});
		    $("#"+padre+" input[name*='tarde_hora_fin']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackTardeFinLive, showPeriod: true,
		    showLeadingZero: true});

		   
				//}else	
					//mostrar_aviso("Debes usar los formularios que aún están vacios para agregar uno nuevo");
		}
	});


	$(".tarjeta-cita .close").live("click", function(event){
		
		index = $(event.currentTarget).index(".close");		
		//eliminar_formset(target, ".tarjeta-cita", "", index);
		delete_form()

	});
	/* Gestión de formsets de BLOQUES */
	

	target_bloque = "bloque_consulta"
	$(".tarjeta-agregar-bloque").live("click", function(event){		

		target_bloque = $(event.currentTarget).parent();
		var id_bloque = target_bloque.attr('id');		
					
		if(is_form_empty(".bloques",[],[])){
			mostrar_aviso("Debes usar los formularios que aún están vacios para agregar uno nuevo");
		}else{
			html = $("#" + id_bloque +  " .model-form").html();			
			var n = agregar_formset_antes(id_bloque, "#" + id_bloque, html, "#" + id_bloque + " .tarjeta-agregar-bloque");


					
			//console.log($(consulta));
			padre = $(this).prev().children('input').parent().attr('id')
			
			$("#"+padre+" input[name*='manana_hora_inicio']").removeClass('hasTimepicker')
			$("#"+padre+" input[name*='manana_hora_inicio']").timepicker({ defaultTime: '12:00', hourText: 'Horas', minuteText: 'Minutos', onHourShow: hora_manana, showPeriod: true,
    showLeadingZero: true});
			$("#"+padre+" input[name*='tarde_hora_inicio']").removeClass('hasTimepicker')

			$("#"+padre+" input[name*='tarde_hora_inicio']").timepicker({defaultTime: '23:55', hourText: 'Horas', minuteText: 'Minutos', onHourShow: hora_tarde, showPeriod: true,
		    showLeadingZero: true});
		    $("#"+padre+" input[name*='tarde_hora_fin']").removeClass('hasTimepicker')
		    $("#"+padre+" input[name*='tarde_hora_fin']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackTardeFinLive, showPeriod: true,
		    showLeadingZero: true});
		    $("#"+padre+" input[name*='manana_hora_fin']").removeClass('hasTimepicker')	
			$("#"+padre+" input[name*='manana_hora_fin']").timepicker({ hourText: 'Horas', minuteText: 'Minutos', onHourShow: OnHourShowCallbackMananaFinLive, showPeriod: true,onMinuteShow: OnMinuteShowCallbackManana,
    showLeadingZero: true});

			

			
		}
		
	});

	$(".tarjeta-bloque .close").live("click", function(event){
		
		index = $(event.currentTarget).index(".close");		
		eliminar_formset(target_bloque, ".tarjeta-bloques", "", index);

	});

/******* ACTUALIZAR SELECT DE SERVICIOS CON EL VALOR DEL INPUT   *******/
	$('#filtro').on('change',function(event) {
		elemento = "#"+ $(this).attr('select')
		valor= $('#filtro').val()
		$( elemento + " option" ).each(function(index, el) {
			if($(this).text()==valor){
				$(this).attr("selected",true);
				return false
			}
		});

	});
})


function OnHourShowCallbackMananaFinLive(hour) {
	/*   SELECCIONANDO EL ID DE LA HORA  */

	elemento = $(this).parent().parent().prev().children('div').children('input').attr('id')
	//console.log("html del input mañana "+ $(this).parent().parent().prev().children('div').html())

	//console.log("#"+elemento+" input[name*='manana_hora_inicio']")
	
    if ((hour <  $("#"+elemento).timepicker('getHour') + 1) || (hour > 12)  ) {
        return false; // not valid
    }
    return true; // valid
}
function OnHourShowCallbackMananaLive(hour) {

	//alert($(this).parent().parent().html())
    if ((hour > 11) || (hour < 6)) {
        return false; // not valid
    }
    return true; // valid
}
function OnHourShowCallbackTardeLive(hour) {
	//alert($(this).parent().parent().html())
    if ((hour < 13) || (hour < 6)) {
        return false; // not valid
    }
    return true; // valid
}
function OnHourShowCallbackTardeFinLive(hour) {
	elemento = $(this).parent().parent().prev().children('div').children('input').attr('id')
	//alert($(this).parent().parent().html())
    if ((hour <  $("#"+elemento).timepicker('getHour') + 1)) {
        return false; // not valid
    }
    return true; // valid
}
function OnMinuteShowCallbackManana(hour, minute) {
	//alert($(this).parent().parent().html())
if ((hour == 12) && (minute > 0)){ return false; } // not valid
return true;  // valid
}


function hora_manana(hour) {
	/*   SELECCIONANDO EL ID DE LA HORA  */
     if ((hour > 11) || (hour < 6)) {
        return false; // not valid
    }
    return true; // valid
}



function hora_tarde(hour) {
	//alert($(this).parent().parent().html())
    if ((hour < 13) || (hour < 6)) {
        return false; // not valid
    }
    return true; // valid
}