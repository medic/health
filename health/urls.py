#encoding:utf-8

from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from django.contrib import admin

from django.conf import settings
admin.autodiscover()

# Definición de URLs generales del proyecto

urlpatterns = patterns('',              

    url(r'^health$', 'base.views.inicio', name='inicio'), 


    url(r'^health/login', 'base.views.acceso'), 
    url(r'^health/logout', 'base.views.cerrar'), 

	url(r'^health/datamark', include('datamark.urls')),
	url(r'^health/pollmark', include('pollmark.urls')), 	
    url(r'^', include('medic.urls')),   

    # URLs de doctores y establecimientos

    url(r'^(?P<user>\w+)$', 'medic.views.perfil', name='perfil'), 

    # Enlaces para el módulo base

	url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^cambio_ganancia/$', 'base.views.cambio_ganancia'), 
    url(r'^depositos/$', 'base.views.depositos'), 
    url(r'^ganancias/$', 'base.views.estado_ganancias'), 
    url(r'^citas/registro$', 'base.views.registrar_cita'),

    #  Icono de la aplicación 
    (r'^favicon\.ico$', RedirectView.as_view(url='/static_health/img/favicon.ico')),

    # Archivo para crawlers
    (r'^robots\.txt$', RedirectView.as_view(url='/static_health/robots.txt')),

    #  Enlace al directorio de subida de archivos. 

    url(r'^media/(?P<path>.*)$','django.views.static.serve',
		{'document_root':settings.MEDIA_ROOT,}),

    
)