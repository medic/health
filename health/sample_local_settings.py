#encoding:utf-8

# Configuraciones Django 

import os


DEBUG = True
TEMPLATE_DEBUG = DEBUG

# En caso de integrase Health con la base de datos en MongoDB colocar está variable en True
WITH_MONGODB = False

# Desarrolladores del proyecto 

ADMINS = (
    ('Carlos', 'cpinelly@gmail.com'),        
    ('Christian', 'cjvaldivieso@gmail.com'),
    ('Evert', 'evert.ortiz.m@gmail.com')
)

MANAGERS = ADMINS

# Ruta absoluta del proyecto en el computador

RUTA_PROYECTO = os.path.dirname(os.path.realpath(__file__))

# Bases de datos a utilizar el proyecto


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'medic_health',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        },
        'mse_db':{
         'ENGINE' :'django_mongodb_engine',
         'NAME': 'af_pegaso-ics2040',
         'USER': 'health',                      
         'PASSWORD': 'cp0311',                 
         'HOST': 'ds049858.mongolab.com',                      
         'PORT': 49858
    }
}

# Configuraciones del MSE

MSE = {
    'host': 'localhost',
    'port': 8008
}

# Cofiguraciones de correo

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'cpinelly@gmail.com'
EMAIL_HOST_PASSWORD = 'cp20160205'
EMAIL_PORT = 587

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Caracas'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'es-ve'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = False

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

TIME_FORMAT = 'H:i p'
TIME_INPUT_FORMATS = ('%H:%M %p',)

# Ruta absoluta del directorio a donde se guardarán los archivos subidos
MEDIA_ROOT = os.path.join( RUTA_PROYECTO, 'media')

#En caso de de producción, debería ser esata ruta
#MEDIA_ROOT = '/home/medic/health/public/media_health'

# URL que maneja la media desde MEDIA_ROOT. 
# Ejemplos: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# Dirección en el navegador para los archivos estáticos.
STATIC_URL = '/static_health/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join( RUTA_PROYECTO, 'static'),
    #En caso de producción debería activarse la línea de abajo
    #'/home/medic/health/public/static_health'
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '3udocdh^d1g^yt&amp;g^hh1cv0k^fyd&amp;%)&amp;97&amp;$ne*5gom*!_#y91'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)


# Procesadores de contexto 

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",

    # Procesadores propios de Health 
    'base.context_processors.integrante',
)

DATABASE_ROUTERS = (
    'datamark_mdb.router.MSERouter',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

SESSION_COOKIE_HTTPONLY = False


# Modelo usado para la gestión de usuario de Django
AUTH_PROFILE_MODULE = 'base.Integrante'

ROOT_URLCONF = 'health.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'health.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.

    os.path.join(RUTA_PROYECTO, 'plantillas'),
    os.path.join(RUTA_PROYECTO, 'plantillas/datamark'),
    os.path.join(RUTA_PROYECTO, 'plantillas/datamark/includes'),
    os.path.join(RUTA_PROYECTO, 'plantillas/pollmark'),
    os.path.join(RUTA_PROYECTO, 'plantillas/boilerplates'),
    os.path.join(RUTA_PROYECTO, 'plantillas/medic')
)

# URL de la página de logeo, usado por el módulo de Authentication
LOGIN_URL = '/login'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',    
    'django.contrib.admin',    
    'django.contrib.admindocs',
    # Módulo para migraciones
    'south',

    #  --- Módulos de Health --- 

    'base',     # Módulo que posee componentes generales de Health
    'datamark', # Módulo de Análisis del Mercado 
    'medic',    # Servicios para los usuarios            
    
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
