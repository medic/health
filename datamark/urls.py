#encoding:utf-8

from django.conf.urls import patterns, url

from datamark import views
from django.views.generic import TemplateView
from models import *
from views import *

#importes para tastypie
from django.conf.urls.defaults import *
from tastypie.api import Api
from datamark.api import EspecialidadResource, EstablecimientoResource, DoctorResource, ServicioResource, TelefonoResource

#recurso para tastypie
especialidad_resource = EspecialidadResource()
tdoctor_resource = DoctorResource()
establecimiento_resource = EstablecimientoResource()
servpub_resource = ServicioResource()
telefono_doctor_resource = TelefonoResource()
# Definición de URLs para Datamark

urlpatterns = patterns('',
    url(r'^$', views.inicio ), 

    # URL para gestión de establecimientos

    url(r'^establecimiento/(?P<id_establecimiento>\d+)$',views.detalle_establecimiento),
    url(r'^establecimiento/nuevo/$',views.establecimiento),
    #url(r'^establecimiento/replica/$',views.replicar_establecimiento),
    url(r'^establecimiento/editar/(?P<id_establecimiento>\d+)$',views.establecimiento),    
    url(r'^establecimiento/eliminar/(?P<id_establecimiento>\d+)$',views.eliminar_establecimiento),
    url(r'^establecimientos/$',views.establecimientos),      
    
    # URL para gestión de Zonas

    url(r'^zonas/$',views.zonas),
    url(r'^zonas/nuevo$',views.nueva_zona),
    url(r'^zonas/(?P<id_zona>\d+)$',views.nueva_zona),
    url(r'^zona/(?P<id_zona>\d+)$',views.detalle_zona),
    url(r'^zonas/eliminar/(?P<id_zona>\d+)$',views.eliminar_zona),
    
    # URL para gestión de doctores

    url(r'^doctores/$',views.doctores),
    url(r'^doctor/nuevo$',views.doctor),
    url(r'^doctor/editar/(?P<id_doctor>\d+)$',views.doctor),
    url(r'^doctor/view/(?P<id_doctor>\d+)$',views.detalle_doctor),
    url(r'^doctor/eliminar/(?P<id_doctor>\d+)$',views.eliminar_doctor),
    url(r'^doctor/disponible$',views.usuario_doctor_disponible),
    url(r'^doctor/(?P<id_doctor>\d+)$',views.detalle_doctor),
   
    #URL para gestion de especialidades  

    #con tastypie
    (r'^api/', include(especialidad_resource.urls)),
    (r'^api/', include(establecimiento_resource.urls)),
    (r'^api/', include(tdoctor_resource.urls)),
    (r'^api/', include(servpub_resource.urls)),
    (r'^api/', include(telefono_doctor_resource.urls)),
    
    url(r'^especialidades/$',views.especialidades),

    # URLs para medios publicitarios
    
    url(r'^medios_publicitarios/$',views.medios_publicitarios),
    url(r'^medio_publicitario/nuevo$',views.nuevo_medio_publicitario),
    url(r'^medio_publicitario/editar/(?P<id_medio>\d+)$',views.edicion_medio_publicitario),    

    # URL para Empresas de Seguros
    url(r'^seguros/$', SeguroListView.as_view(),name='seguros'),
    url(r'^seguro/add/$', SeguroCreateView.as_view(),name='nuevo_seguro'),
    url(r'^seguro/editar/(?P<pk>\d+)/$', SeguroUpdateView.as_view(),name='editar_seguro'),
    url(r'^seguro/detalle/(?P<pk>\d+)/$', SeguroDetailView.as_view(),name='detalle_seguro'),
    url(r'^seguro/delete/(?P<pk>\d+)/$', SeguroDeleteView.as_view(),name='eliminar_seguro'),

)