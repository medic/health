# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Consulta', fields ['telefono_consultorio']
        db.delete_unique('datamark_consulta', ['telefono_consultorio'])

        # Adding field 'Establecimiento.usuario'
        db.add_column('datamark_establecimiento', 'usuario',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=45),
                      keep_default=False)

        # Adding field 'Establecimiento.presentacion'
        db.add_column('datamark_establecimiento', 'presentacion',
                      self.gf('django.db.models.fields.TextField')(default='', max_length=140, blank=True),
                      keep_default=False)

        # Adding field 'Establecimiento.agregado_el'
        db.add_column('datamark_establecimiento', 'agregado_el',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2013, 8, 16, 0, 0), blank=True),
                      keep_default=False)


        # Changing field 'Establecimiento.web'
        db.alter_column('datamark_establecimiento', 'web', self.gf('django.db.models.fields.CharField')(max_length=60))

        # Changing field 'Establecimiento.servicios'
        db.alter_column('datamark_establecimiento', 'servicios', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Establecimiento.twitter'
        db.alter_column('datamark_establecimiento', 'twitter', self.gf('django.db.models.fields.CharField')(max_length=15))

        # Changing field 'Establecimiento.direccion'
        db.alter_column('datamark_establecimiento', 'direccion', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Bloque_Consulta.dias'
        db.alter_column('datamark_bloque_consulta', 'dias', self.gf('django.db.models.fields.CharField')(max_length=32))

        # Changing field 'Consulta.extension_telefono'
        db.alter_column('datamark_consulta', 'extension_telefono', self.gf('django.db.models.fields.CharField')(default='', max_length=4))

        # Changing field 'Consulta.telefono_consultorio'
        db.alter_column('datamark_consulta', 'telefono_consultorio', self.gf('django.db.models.fields.CharField')(default='', max_length=11))
        # Adding unique constraint on 'Especialidad', fields ['especialidad']
        db.create_unique('datamark_especialidad', ['especialidad'])


    def backwards(self, orm):
        # Removing unique constraint on 'Especialidad', fields ['especialidad']
        db.delete_unique('datamark_especialidad', ['especialidad'])

        # Deleting field 'Establecimiento.usuario'
        db.delete_column('datamark_establecimiento', 'usuario')

        # Deleting field 'Establecimiento.presentacion'
        db.delete_column('datamark_establecimiento', 'presentacion')

        # Deleting field 'Establecimiento.agregado_el'
        db.delete_column('datamark_establecimiento', 'agregado_el')


        # Changing field 'Establecimiento.web'
        db.alter_column('datamark_establecimiento', 'web', self.gf('django.db.models.fields.CharField')(max_length=45))

        # Changing field 'Establecimiento.servicios'
        db.alter_column('datamark_establecimiento', 'servicios', self.gf('django.db.models.fields.TextField')(max_length=500))

        # Changing field 'Establecimiento.twitter'
        db.alter_column('datamark_establecimiento', 'twitter', self.gf('django.db.models.fields.CharField')(max_length=20))

        # Changing field 'Establecimiento.direccion'
        db.alter_column('datamark_establecimiento', 'direccion', self.gf('django.db.models.fields.TextField')(max_length=200))

        # Changing field 'Bloque_Consulta.dias'
        db.alter_column('datamark_bloque_consulta', 'dias', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=14))

        # Changing field 'Consulta.extension_telefono'
        db.alter_column('datamark_consulta', 'extension_telefono', self.gf('django.db.models.fields.CharField')(max_length=4, null=True))

        # Changing field 'Consulta.telefono_consultorio'
        db.alter_column('datamark_consulta', 'telefono_consultorio', self.gf('django.db.models.fields.CharField')(unique=True, max_length=11, null=True))
        # Adding unique constraint on 'Consulta', fields ['telefono_consultorio']
        db.create_unique('datamark_consulta', ['telefono_consultorio'])


    models = {
        'datamark.bloque_atencion': {
            'Meta': {'object_name': 'Bloque_atencion'},
            'dia': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manana_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'manana_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'tarde_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'tarde_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'})
        },
        'datamark.bloque_consulta': {
            'Meta': {'object_name': 'Bloque_Consulta'},
            'consulta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Consulta']"}),
            'dias': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manana_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'manana_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tarde_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tarde_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        'datamark.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'codigo_area': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'datamark.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'detalle_sitio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Doctor']"}),
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']", 'null': 'True', 'blank': 'True'}),
            'extension_telefono': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'previa_cita': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'telefono_consultorio': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'})
        },
        'datamark.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'especialidades': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Especialidad']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'titulo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        'datamark.especialidad': {
            'Meta': {'object_name': 'Especialidad'},
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'especialidad': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre_especialista': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'palabras_claves': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'datamark.establecimiento': {
            'Meta': {'object_name': 'Establecimiento'},
            'agregado_el': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {}),
            'doctor': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Doctor']", 'through': "orm['datamark.Consulta']", 'symmetrical': 'False'}),
            'fecha_inauguracion': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'max_length': '140', 'blank': 'True'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'servicios_publicitarios': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Servicio_Publicitario']", 'symmetrical': 'False', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'todo_momento': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Zona']", 'null': 'True'})
        },
        'datamark.medio_publicitario': {
            'Meta': {'object_name': 'Medio_Publicitario'},
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        'datamark.servicio_publicitario': {
            'Meta': {'object_name': 'Servicio_Publicitario'},
            'costo': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'costo_real': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio_publicitario': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Medio_Publicitario']"})
        },
        'datamark.telefono_doctor': {
            'Meta': {'object_name': 'Telefono_Doctor'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Doctor']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'unique': 'True', 'null': 'True'})
        },
        'datamark.telefono_establecimiento': {
            'Meta': {'object_name': 'Telefono_Establecimiento'},
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True'})
        },
        'datamark.zona': {
            'Meta': {'object_name': 'Zona'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Ciudad']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['datamark']