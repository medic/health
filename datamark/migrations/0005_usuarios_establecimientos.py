# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."

        for establecimiento in orm.Establecimiento.objects.all():
            usuario = establecimiento.nombre            
            usuario = usuario.replace(u'ó', 'o')
            usuario = usuario.replace(u'ú', 'u')
            usuario = usuario.replace(u'í', 'i')
            usuario = usuario.replace(u'á', 'a')
            usuario = usuario.replace(u'é', 'e')
            usuario = usuario.replace(u'Á', 'A')
            usuario = usuario.replace(u'Ó', 'O')
            usuario = usuario.replace(u'É', 'E')
            usuario = usuario.replace(u'Ú', 'U')
            usuario = usuario.replace(u'Í', 'I')
            usuario = usuario.replace('.', '')
            usuario = usuario.replace(',', '')
            usuario = usuario.replace(' ', '')
            usuario = usuario.lower()
            establecimiento.usuario = usuario
            establecimiento.save()

        # Note: Remember to use orm['appname.ModelName'] rather than "from appname.models..."

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'datamark.bloque_atencion': {
            'Meta': {'object_name': 'Bloque_atencion'},
            'dia': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manana_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'manana_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'tarde_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'tarde_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'})
        },
        'datamark.bloque_consulta': {
            'Meta': {'object_name': 'Bloque_Consulta'},
            'consulta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Consulta']"}),
            'dias': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manana_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'manana_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tarde_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tarde_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        'datamark.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'codigo_area': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'datamark.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'detalle_sitio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Doctor']"}),
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']", 'null': 'True', 'blank': 'True'}),
            'extension_telefono': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'previa_cita': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'telefono_consultorio': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'})
        },
        'datamark.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'especialidades': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Especialidad']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'titulo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        'datamark.especialidad': {
            'Meta': {'object_name': 'Especialidad'},
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'especialidad': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre_especialista': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'palabras_claves': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'datamark.establecimiento': {
            'Meta': {'object_name': 'Establecimiento'},
            'agregado_el': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {}),
            'doctor': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Doctor']", 'through': "orm['datamark.Consulta']", 'symmetrical': 'False'}),
            'fecha_inauguracion': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'max_length': '140', 'blank': 'True'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'servicios_publicitarios': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Servicio_Publicitario']", 'symmetrical': 'False', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'todo_momento': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Zona']", 'null': 'True'})
        },
        'datamark.medio_publicitario': {
            'Meta': {'object_name': 'Medio_Publicitario'},
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        'datamark.servicio_publicitario': {
            'Meta': {'object_name': 'Servicio_Publicitario'},
            'costo': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'costo_real': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio_publicitario': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Medio_Publicitario']"})
        },
        'datamark.telefono_doctor': {
            'Meta': {'object_name': 'Telefono_Doctor'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Doctor']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'unique': 'True', 'null': 'True'})
        },
        'datamark.telefono_establecimiento': {
            'Meta': {'object_name': 'Telefono_Establecimiento'},
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True'})
        },
        'datamark.zona': {
            'Meta': {'object_name': 'Zona'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Ciudad']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['datamark']
    symmetrical = True
