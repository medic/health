# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Doctor.rif'
        db.add_column(u'datamark_doctor', 'rif',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=13, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Doctor.rif'
        db.delete_column(u'datamark_doctor', 'rif')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'codename',)", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'datamark.bloque_atencion': {
            'Meta': {'object_name': 'Bloque_Atencion'},
            'dias': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Establecimiento']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manana_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'manana_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'tarde_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'}),
            'tarde_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True'})
        },
        u'datamark.bloque_consulta': {
            'Meta': {'object_name': 'Bloque_Consulta'},
            'consulta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Consulta']"}),
            'dias': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manana_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'manana_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tarde_hora_fin': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'tarde_hora_inicio': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        u'datamark.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'codigo_area': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'datamark.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'detalle_sitio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Doctor']"}),
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Establecimiento']", 'null': 'True', 'blank': 'True'}),
            'extension_telefono': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'previa_cita': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'telefono_consultorio': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'})
        },
        u'datamark.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'adicional': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'agregado_el': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'especialidades': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['datamark.Especialidad']", 'symmetrical': 'False'}),
            'facebook': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mse_id': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'revisado_de': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Medio_Publicitario']"}),
            'revisado_el': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'revisado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'titulo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'datamark.especialidad': {
            'Meta': {'object_name': 'Especialidad'},
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'especialidad': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mse_id': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'nombre_especialista': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'palabras_claves': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'datamark.establecimiento': {
            'Meta': {'object_name': 'Establecimiento'},
            'adicional': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'agregado_el': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'descripcion_corta': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {}),
            'doctores': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['datamark.Doctor']", 'through': u"orm['datamark.Consulta']", 'symmetrical': 'False'}),
            'facebook': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_inauguracion': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mse_id': ('django.db.models.fields.CharField', [], {'max_length': '25', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'max_length': '140', 'blank': 'True'}),
            'revisado_de': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['datamark.Medio_Publicitario']"}),
            'revisado_el': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'revisado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'servicios_publicitarios': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['datamark.Servicio_Publicitario']", 'symmetrical': 'False', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'todo_momento': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Zona']", 'null': 'True'})
        },
        u'datamark.medio_publicitario': {
            'Meta': {'object_name': 'Medio_Publicitario'},
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'max_length': '45', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        u'datamark.servicio_publicitario': {
            'Meta': {'object_name': 'Servicio_Publicitario'},
            'costo': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'costo_real': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio_publicitario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Medio_Publicitario']"})
        },
        u'datamark.telefono_doctor': {
            'Meta': {'object_name': 'Telefono_Doctor'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'unique': 'True', 'null': 'True'})
        },
        u'datamark.telefono_establecimiento': {
            'Meta': {'object_name': 'Telefono_Establecimiento'},
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Establecimiento']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True'})
        },
        u'datamark.zona': {
            'Meta': {'object_name': 'Zona'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['datamark.Ciudad']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['datamark']