# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Especialidad'
        db.create_table('datamark_especialidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('especialidad', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('nombre_especialista', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
            ('palabras_claves', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('datamark', ['Especialidad'])

        # Adding model 'Doctor'
        db.create_table('datamark_doctor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('apellido', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('usuario', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('presentacion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('sexo', self.gf('django.db.models.fields.CharField')(default='DC', max_length=2)),
            ('titulo', self.gf('django.db.models.fields.CharField')(default='DC', max_length=2)),
            ('correo', self.gf('django.db.models.fields.EmailField')(max_length=45, blank=True)),
            ('twitter', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('web', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
            ('servicios', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('datamark', ['Doctor'])

        # Adding M2M table for field especialidades on 'Doctor'
        db.create_table('datamark_doctor_especialidades', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('doctor', models.ForeignKey(orm['datamark.doctor'], null=False)),
            ('especialidad', models.ForeignKey(orm['datamark.especialidad'], null=False))
        ))
        db.create_unique('datamark_doctor_especialidades', ['doctor_id', 'especialidad_id'])

        # Adding model 'Telefono_Doctor'
        db.create_table('datamark_telefono_doctor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=11, unique=True, null=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Doctor'])),
        ))
        db.send_create_signal('datamark', ['Telefono_Doctor'])

        # Adding model 'Medio_Publicitario'
        db.create_table('datamark_medio_publicitario', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('rif', self.gf('django.db.models.fields.CharField')(max_length=13, blank=True)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(max_length=45, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=11, blank=True)),
            ('correo', self.gf('django.db.models.fields.EmailField')(max_length=45, blank=True)),
            ('twitter', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('web', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
        ))
        db.send_create_signal('datamark', ['Medio_Publicitario'])

        # Adding model 'Servicio_Publicitario'
        db.create_table('datamark_servicio_publicitario', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('costo', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=8, decimal_places=2)),
            ('costo_real', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('medio_publicitario', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Medio_Publicitario'])),
        ))
        db.send_create_signal('datamark', ['Servicio_Publicitario'])

        # Adding model 'Ciudad'
        db.create_table('datamark_ciudad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('codigo_area', self.gf('django.db.models.fields.CharField')(max_length=4, blank=True)),
        ))
        db.send_create_signal('datamark', ['Ciudad'])

        # Adding model 'Zona'
        db.create_table('datamark_zona', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('ciudad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Ciudad'])),
        ))
        db.send_create_signal('datamark', ['Zona'])

        # Adding model 'Establecimiento'
        db.create_table('datamark_establecimiento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('rif', self.gf('django.db.models.fields.CharField')(max_length=13, blank=True)),
            ('direccion', self.gf('django.db.models.fields.TextField')(max_length=200)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('fecha_inauguracion', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('correo', self.gf('django.db.models.fields.EmailField')(max_length=45, blank=True)),
            ('servicios', self.gf('django.db.models.fields.TextField')(max_length=500, blank=True)),
            ('todo_momento', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('web', self.gf('django.db.models.fields.CharField')(max_length=45, blank=True)),
            ('twitter', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('zona', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Zona'], null=True)),
        ))
        db.send_create_signal('datamark', ['Establecimiento'])

        # Adding M2M table for field servicios_publicitarios on 'Establecimiento'
        db.create_table('datamark_establecimiento_servicios_publicitarios', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('establecimiento', models.ForeignKey(orm['datamark.establecimiento'], null=False)),
            ('servicio_publicitario', models.ForeignKey(orm['datamark.servicio_publicitario'], null=False))
        ))
        db.create_unique('datamark_establecimiento_servicios_publicitarios', ['establecimiento_id', 'servicio_publicitario_id'])

        # Adding model 'Consulta'
        db.create_table('datamark_consulta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Doctor'])),
            ('establecimiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Establecimiento'])),
            ('detalle_sitio', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('telefono_consultorio', self.gf('django.db.models.fields.CharField')(max_length=11, unique=True, null=True)),
            ('previa_cita', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('extension_telefono', self.gf('django.db.models.fields.CharField')(max_length=4, null=True)),
        ))
        db.send_create_signal('datamark', ['Consulta'])

        # Adding model 'Telefono_Establecimiento'
        db.create_table('datamark_telefono_establecimiento', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=11, null=True)),
            ('establecimiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Establecimiento'])),
        ))
        db.send_create_signal('datamark', ['Telefono_Establecimiento'])

        # Adding model 'Bloque_atencion'
        db.create_table('datamark_bloque_atencion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dia', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('hora_inicio', self.gf('django.db.models.fields.TimeField')()),
            ('hora_fin', self.gf('django.db.models.fields.TimeField')()),
            ('establecimiento', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Establecimiento'])),
        ))
        db.send_create_signal('datamark', ['Bloque_atencion'])

        # Adding model 'Bloque_Consulta'
        db.create_table('datamark_bloque_consulta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dia', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('hora_inicio', self.gf('django.db.models.fields.TimeField')()),
            ('hora_fin', self.gf('django.db.models.fields.TimeField')()),
            ('consulta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['datamark.Consulta'])),
        ))
        db.send_create_signal('datamark', ['Bloque_Consulta'])


    def backwards(self, orm):
        # Deleting model 'Especialidad'
        db.delete_table('datamark_especialidad')

        # Deleting model 'Doctor'
        db.delete_table('datamark_doctor')

        # Removing M2M table for field especialidades on 'Doctor'
        db.delete_table('datamark_doctor_especialidades')

        # Deleting model 'Telefono_Doctor'
        db.delete_table('datamark_telefono_doctor')

        # Deleting model 'Medio_Publicitario'
        db.delete_table('datamark_medio_publicitario')

        # Deleting model 'Servicio_Publicitario'
        db.delete_table('datamark_servicio_publicitario')

        # Deleting model 'Ciudad'
        db.delete_table('datamark_ciudad')

        # Deleting model 'Zona'
        db.delete_table('datamark_zona')

        # Deleting model 'Establecimiento'
        db.delete_table('datamark_establecimiento')

        # Removing M2M table for field servicios_publicitarios on 'Establecimiento'
        db.delete_table('datamark_establecimiento_servicios_publicitarios')

        # Deleting model 'Consulta'
        db.delete_table('datamark_consulta')

        # Deleting model 'Telefono_Establecimiento'
        db.delete_table('datamark_telefono_establecimiento')

        # Deleting model 'Bloque_atencion'
        db.delete_table('datamark_bloque_atencion')

        # Deleting model 'Bloque_Consulta'
        db.delete_table('datamark_bloque_consulta')


    models = {
        'datamark.bloque_atencion': {
            'Meta': {'object_name': 'Bloque_atencion'},
            'dia': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']"}),
            'hora_fin': ('django.db.models.fields.TimeField', [], {}),
            'hora_inicio': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'datamark.bloque_consulta': {
            'Meta': {'object_name': 'Bloque_Consulta'},
            'consulta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Consulta']"}),
            'dia': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'hora_fin': ('django.db.models.fields.TimeField', [], {}),
            'hora_inicio': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'datamark.ciudad': {
            'Meta': {'object_name': 'Ciudad'},
            'codigo_area': ('django.db.models.fields.CharField', [], {'max_length': '4', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'datamark.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'detalle_sitio': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Doctor']"}),
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']"}),
            'extension_telefono': ('django.db.models.fields.CharField', [], {'max_length': '4', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'previa_cita': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'telefono_consultorio': ('django.db.models.fields.CharField', [], {'max_length': '11', 'unique': 'True', 'null': 'True'})
        },
        'datamark.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'especialidades': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Especialidad']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'presentacion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'sexo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'titulo': ('django.db.models.fields.CharField', [], {'default': "'DC'", 'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        'datamark.especialidad': {
            'Meta': {'object_name': 'Especialidad'},
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'especialidad': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre_especialista': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'palabras_claves': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'datamark.establecimiento': {
            'Meta': {'object_name': 'Establecimiento'},
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'doctor': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Doctor']", 'through': "orm['datamark.Consulta']", 'symmetrical': 'False'}),
            'fecha_inauguracion': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'servicios': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            'servicios_publicitarios': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['datamark.Servicio_Publicitario']", 'symmetrical': 'False', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'todo_momento': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'}),
            'zona': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Zona']", 'null': 'True'})
        },
        'datamark.medio_publicitario': {
            'Meta': {'object_name': 'Medio_Publicitario'},
            'correo': ('django.db.models.fields.EmailField', [], {'max_length': '45', 'blank': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'max_length': '45', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'rif': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'web': ('django.db.models.fields.CharField', [], {'max_length': '45', 'blank': 'True'})
        },
        'datamark.servicio_publicitario': {
            'Meta': {'object_name': 'Servicio_Publicitario'},
            'costo': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '8', 'decimal_places': '2'}),
            'costo_real': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio_publicitario': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Medio_Publicitario']"})
        },
        'datamark.telefono_doctor': {
            'Meta': {'object_name': 'Telefono_Doctor'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Doctor']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'unique': 'True', 'null': 'True'})
        },
        'datamark.telefono_establecimiento': {
            'Meta': {'object_name': 'Telefono_Establecimiento'},
            'establecimiento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Establecimiento']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True'})
        },
        'datamark.zona': {
            'Meta': {'object_name': 'Zona'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['datamark.Ciudad']"}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['datamark']