# -*- coding: utf-8 -*-

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView

from django.views.generic import TemplateView,FormView,ListView
from django.views.generic.base import View, RedirectView
from django.core.urlresolvers import reverse_lazy


from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings

from forms import *
from models import *
from consts import DIAS_OPCIONES

from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from django.db.models import Q
from django.db.models import Count

from django.core import serializers

# Uso de JSON 
import json

#Estandar de Python para expresiones regulares
import re 	

@login_required
def inicio(request):
	n_doctores=Doctor.objects.all().count()
	n_especialidades=Especialidad.objects.all().count()
	n_establecimientos=Establecimiento.objects.all().count()
	n_medios_publicitarios=Medio_Publicitario.objects.all().count()
	n_zonas=Zona.objects.all().count()
	n_ciudades=Ciudad.objects.all().count()
	val_establecimientos= Establecimiento.objects.exclude(validado_por_id=None).count()
	val_doctores= Doctor.objects.exclude(validado_por_id=None).count()
	usuarios = User.objects.filter(is_active=True).annotate(establecimientos_count=Count('establecimiento', distinct=True), doctores_count=Count('doctor', distinct=True) ).filter(Q(establecimientos_count__gt=0) | Q(doctores_count__gt=0)) 				

	for u in usuarios:
		u.general_count = u.establecimientos_count + u.doctores_count

	usuarios = sorted(usuarios, key=lambda usuario: usuario.general_count, reverse=True)
	res_establecimientos = n_establecimientos -val_establecimientos
	res_doctores = n_doctores - val_doctores

	return render_to_response('dm-inicio.html', 
		{'n_doctores': n_doctores,
		'n_especialidades': n_especialidades,
		'n_establecimientos': n_establecimientos,
		'n_medios_publicitarios': n_medios_publicitarios,
		'n_zonas': n_zonas,
		'n_ciudades':n_ciudades,
		'ranking_cargas': usuarios,
		'val_establecimientos':val_establecimientos,
		'res_establecimientos':res_establecimientos,
		'res_doctores':res_doctores,
		'val_doctores':val_doctores
		},
		context_instance=RequestContext(request))

@login_required
def  establecimiento(request,id_establecimiento=None):
	"""
		Vista usada para registrar un establecimiento y editarlo.
	"""		
	telefonos_prefix = 'telefonos'
	horarios_prefix = 'horarios'	

	# Indica si el registro será replicado, es decir, si debe generarse un nuevo formulario
	replicar = True if '_replicar' in request.POST else False

	# Indica si el formulario debe guardarse como un nuevo registro
	replicado = True if '_replicado' in request.POST else False
	validar = True if '_validar' in request.POST else False


	establecimiento = None
	if  request.method=='POST':	
		#establecimiento_form = EstablecimientoForm(request.POST)			

		if id_establecimiento is not None and not replicado:

			"""Actualización de un establecimiento"""

			establecimiento = get_object_or_404(Establecimiento,pk=id_establecimiento)
		#	establecimiento_form.instance = establecimiento

			formulario =EstablecimientoForm(request.POST,instance=establecimiento)
			telefonos = TelefonoEstablecimientoFormSet(request.POST, instance=establecimiento, prefix=telefonos_prefix)
			horarios_atencion = BloqueAtencionFormset(request.POST, instance=establecimiento, prefix=horarios_prefix)
											
		else:
			"""Registro de un establecimiento"""

			formulario=EstablecimientoForm(request.POST)
			telefonos = TelefonoEstablecimientoFormSet(request.POST, prefix=telefonos_prefix)
			horarios_atencion = BloqueAtencionFormset(request.POST, prefix=horarios_prefix)

		if formulario.is_valid() and telefonos.is_valid() and horarios_atencion.is_valid():			

			if validar:
				establecimiento = formulario.save(commit=False)
				establecimiento.revisado_por = request.user
				establecimiento.validado_el = datetime.datetime.now()
				establecimiento.validado_por= request.user
				establecimiento.save()	

				telefonos.instance = establecimiento				
				telefonos.save()

				establecimiento.save(mse_commit=False)

				formulario.save_m2m()
			

				if not establecimiento.todo_momento:
					horarios_atencion.instance = establecimiento			
					horarios_atencion.save()		

				if replicar:
					establecimiento.pk = None
					#telefonos_initial = [{key.split("-")[-1]: value} for key, value in request.POST.iteritems() if key.find(telefonos_prefix) > -1 and not key.endswith("-id")]								

					formulario = EstablecimientoForm(instance=establecimiento)				
					telefonos = TelefonoEstablecimientoFormSet(prefix=telefonos_prefix)
					horarios_atencion = BloqueAtencionFormset(prefix=horarios_prefix)
					establecimiento = None

				else:
					return redirect ('datamark.views.establecimientos')	
				
			else:
				
				establecimiento = formulario.save(commit=False)
				establecimiento.revisado_por = request.user
				establecimiento.save()	

				telefonos.instance = establecimiento				
				telefonos.save()

				establecimiento.save(mse_commit=False)

				formulario.save_m2m()
			

				if not establecimiento.todo_momento:
					horarios_atencion.instance = establecimiento			
					horarios_atencion.save()		

				if replicar:
					establecimiento.pk = None
					#telefonos_initial = [{key.split("-")[-1]: value} for key, value in request.POST.iteritems() if key.find(telefonos_prefix) > -1 and not key.endswith("-id")]								

					formulario = EstablecimientoForm(instance=establecimiento)				
					telefonos = TelefonoEstablecimientoFormSet(prefix=telefonos_prefix)
					horarios_atencion = BloqueAtencionFormset(prefix=horarios_prefix)
					establecimiento = None

				else:
					return redirect ('datamark.views.establecimientos')		

		else:
			replicar = True if replicado else False				
		
	else:						

		if id_establecimiento is not None:

			"""Preparación de formulario para edición de un establecimiento"""
			
			establecimiento = get_object_or_404(Establecimiento,pk=id_establecimiento)
			telefonos = TelefonoEstablecimientoFormSet(instance=establecimiento, prefix=telefonos_prefix)
			horarios_atencion = BloqueAtencionFormset(instance=establecimiento, prefix=horarios_prefix)
					
			formulario=EstablecimientoForm(instance=establecimiento)
												
		else:

			"""Formulario para registro de un establecimiento"""		
			
			telefonos = TelefonoEstablecimientoFormSet(prefix=telefonos_prefix)	
			horarios_atencion = BloqueAtencionFormset(prefix=horarios_prefix)

			formulario=EstablecimientoForm()

	return render_to_response('establecimientoForm.html',
		{'formulario':formulario, 		
		'telefonos': telefonos,
		'horarios_atencion': horarios_atencion,
		'establecimiento': establecimiento,
		'replicado': replicar},
		context_instance=RequestContext(request))


@login_required
def establecimientos(request):
	""" Listado de establecimientos """

	lista_establecimientos=Establecimiento.objects.all()
	return render_to_response('establecimientos.html',
		{'establecimientos':lista_establecimientos},
		context_instance=RequestContext(request))

@login_required
def detalle_establecimiento(request,id_establecimiento):
	""" Consulta de establecimientos """

	dato 	=	get_object_or_404(Establecimiento,pk=id_establecimiento)	
	lista_doctores = Doctor.objects.filter(consulta__establecimiento__exact=id_establecimiento)
	return render_to_response('establecimiento.html',
		{'establecimiento':dato,'doctores':lista_doctores},
		context_instance=RequestContext(request))

@login_required
def eliminar_establecimiento(request,id_establecimiento):
	""" Eliminacion de establecimientos """

	dato 	=	get_object_or_404(Establecimiento,pk=id_establecimiento)
	
	if dato is not None:
		dato.delete()
		return redirect('datamark.views.establecimientos')

	else:
		return dato

@login_required
def zonas(request):
	lista_zona  = Zona.objects.all()
	return render_to_response('zonas.html',{'zona':lista_zona},context_instance=RequestContext(request))

@login_required
def nueva_zona(request,id_zona=None):

	if request.method=='POST':

		if id_zona is not None:
			zona=get_object_or_404(Zona,pk=id_zona)
			formulario =ZonaForm(request.POST,instance=zona)
		else:
			formulario=ZonaForm(request.POST)

		if formulario.is_valid():
			formulario.save()
			return redirect ('datamark.views.zonas')
	else:
		if id_zona is not None:
			zona=get_object_or_404(Zona,pk=id_zona)
			formulario=ZonaForm(instance=zona)
		else:
			formulario=ZonaForm()
	return render_to_response('zonaForm.html',{'formulario':formulario},context_instance=RequestContext(request))

@login_required
def detalle_zona(request,id_zona):
	zona =get_object_or_404(Zona,pk=id_zona)

	return render_to_response('zona.html', {'zona':zona}, context_instance=RequestContext(request))

@login_required
def eliminar_zona(request,id_zona):
	zona = get_object_or_404(Zona,pk=id_zona)

	if zona is not None:
		if zona.establecimiento_set.count()==0:
			zona.delete()
			return redirect('datamark.views.zonas')

# Vistas para gestión de doctores


@login_required
def doctor(request,id_doctor=None):	

	telefonos_prefix = 'telefonos'
	doctor = None
	
	validar = True if '_validar' in request.POST else False

	if  request.method=='POST':

	
		doctor_form = DoctorForm(request.POST)		
				
		if id_doctor is not None:

			""" Actualización de un doctor """

			doctor = get_object_or_404(Doctor,pk=id_doctor)			
			doctor_form.instance = doctor
			consultas_formset = ConsultaFormset(request.POST, instance=doctor)
			telefonos_formset = TelefonoDoctorFormset(request.POST, prefix=telefonos_prefix, instance=doctor)
		else:			
			# Si no se coloca la instancia, da error
			consultas_formset = ConsultaFormset(request.POST, instance=doctor)
			telefonos_formset = TelefonoDoctorFormset(request.POST, prefix=telefonos_prefix, instance=doctor)		
							
		if  doctor_form.is_valid() and consultas_formset.is_valid() and telefonos_formset.is_valid():			
			
			if validar:	
				
				doctor = doctor_form.save(commit=False)
				doctor.revisado_por = request.user
				doctor.validado_el=datetime.datetime.now()
				doctor.validado_por=request.user
				doctor.save(mse_commit=False)
				doctor_form.save_m2m()
				doctor.save_mse()
				consultas_formset.instance = doctor
				telefonos_formset.instance = doctor
				consultas_formset.save_all()
				telefonos_formset.save()
			
				
				return redirect ('datamark.views.doctores')	
			else:
				
				doctor = doctor_form.save(commit=False)
				doctor.revisado_por = request.user
				doctor.save(mse_commit=False)

			

				doctor_form.save_m2m()
				doctor.save_mse()

				consultas_formset.instance = doctor
				telefonos_formset.instance = doctor
				consultas_formset.save_all()
				telefonos_formset.save()
			

				return redirect ('datamark.views.doctores')	

			
							
	else:

		if id_doctor is not None:
			
			doctor = get_object_or_404(Doctor,pk=id_doctor)

			doctor_form=DoctorForm(instance=doctor)
			consultas_formset = ConsultaFormset(instance=doctor)
			telefonos_formset = TelefonoDoctorFormset(prefix=telefonos_prefix, instance=doctor)												
		else:
			doctor_form = DoctorForm()
			consultas_formset = ConsultaFormset()
			telefonos_formset = TelefonoDoctorFormset(prefix=telefonos_prefix)
			

	return render_to_response('doctorForm.html',
		{'formulario':doctor_form, 
		'consultas': consultas_formset,		
		'telefonos': telefonos_formset,		
		'doctor': doctor
		
		},
		context_instance=RequestContext(request))

@login_required
def usuario_doctor_disponible(request):

	if request.method == 'GET':
		usuario_doctor = request.GET['usuario_doctor']		

		try:
			Doctor.objects.get(usuario=usuario_doctor)	
			return HttpResponse('0')
		except ObjectDoesNotExist:
			return HttpResponse('1')

	raise Http404



@login_required
def doctores(request):
	lista_doctor  = Doctor.objects.all()
	return render_to_response('doctores.html',{'doctor':lista_doctor},context_instance=RequestContext(request))

@login_required
def detalle_doctor(request,id_doctor):
	doctor =get_object_or_404(Doctor,pk=id_doctor)
	consultas = Consulta.objects.filter(doctor=id_doctor)
	return render_to_response('doctor.html', {'doctor':doctor,'consultas':consultas}, context_instance=RequestContext(request))

@login_required
def eliminar_doctor(request,id_doctor):
	doctor = get_object_or_404(Doctor,pk=id_doctor)

	if doctor is not None:
		doctor.delete()
		return redirect('datamark.views.doctores')

	else:
		return dato

# def validar(request,id_doctor):
# 	doctor = get_object_or_404(Doctor,pk=id_doctor)	
# 	doctor.validado_el=datetime.datetime.now()
# 	doctor.validado_por=request.user
# 	doctor = doctor.save()
	
# 	return redirect('datamark.views.doctores')



# Vistas para gestion especialidades


@login_required
def especialidades(request):
	if request.method=='POST':
		formulario = EspecialidadForm(request.POST, request.FILES)
		if formulario.is_valid():
			formulario.save()
	else:
		formulario = EspecialidadForm()

	especialidades = Especialidad.objects.all().order_by('nombre_especialista');

	data = serializers.serialize("json", especialidades);
	
	aux_jsons = json.loads(data)
	respuesta = []

	i = 0
	for e in aux_jsons:
		e['fields']['id'] = e['pk']
		e['fields']['n_especialistas'] = especialidades[i].doctor_set.count()		
		respuesta.append(e['fields'])		
		i = i+1
		
	data = json.dumps(respuesta)

	return render_to_response('especialidades.html',
		{'formulario':formulario,'data':data}, 
		context_instance=RequestContext(request))

# Vistas para gestión de medios publicitarios

@login_required
def medios_publicitarios(request):
	"""Listado de medios publicitarios"""

	medios = Medio_Publicitario.objects.all()

	return render_to_response('medios_publicitarios.html',
		{'medios': medios},
		context_instance=RequestContext(request))

@login_required
def nuevo_medio_publicitario(request):
	"""Registro de un medio publicitario"""

	if request.method == 'POST':

		medio_publicitario_form = MedioPublicitarioForm(request.POST)
		servicios_formset = ServicioPublicitarioFormSet(request.POST)

		if medio_publicitario_form.is_valid() and servicios_formset.is_valid():

			medio_publicitario = medio_publicitario_form.save()

			servicios_formset.instance = medio_publicitario
			servicios_formset.save()			

			return redirect('datamark.views.medios_publicitarios')		

	else:	
		medio_publicitario_form = MedioPublicitarioForm()
		servicios_formset = ServicioPublicitarioFormSet()

	return render_to_response('medio_publicitario_form.html',
		{'medio_publicitario_form': medio_publicitario_form,
		'servicios_formset': servicios_formset},		
		context_instance=RequestContext(request))

@login_required
def edicion_medio_publicitario(request, id_medio):

	medio_publicitario = Medio_Publicitario.objects.get(pk=id_medio)

	if request.method == 'POST':

		medio_publicitario_form = MedioPublicitarioForm(request.POST, instance=medio_publicitario)
		servicios_formset = ServicioPublicitarioFormSet(request.POST, instance=medio_publicitario)

		if medio_publicitario_form.is_valid() and servicios_formset.is_valid():
			medio_publicitario_form.save()
			servicios_formset.save()
			return redirect('datamark.views.medios_publicitarios')

	else:

		medio_publicitario_form = MedioPublicitarioForm(instance=medio_publicitario)
		servicios_formset = ServicioPublicitarioFormSet(instance=medio_publicitario)

	return render_to_response('medio_publicitario_form.html',
		{'medio_publicitario_form': medio_publicitario_form,
		'servicios_formset': servicios_formset},
		context_instance=RequestContext(request))

class SeguroCreateView(CreateView):
	model = Seguro
	form_class=SeguroForm
	context_object_name = 'form'
	template_name='seguroForm.html'
	success_url = reverse_lazy('seguros')
	
	

class SeguroListView(ListView):
	model = Seguro
	template_name='seguros.html'
	context_object_name='seguro'

class SeguroDeleteView(DeleteView):
	model = Seguro
	success_url = reverse_lazy('seguros')

class SeguroUpdateView(UpdateView):
	model = Seguro
	form_class=SeguroForm
	template_name='seguroForm.html'
	success_url = reverse_lazy('seguros')
	context_object_name = 'form'

class SeguroDetailView(DetailView):
	model = Seguro
	template_name='seguro.html'
	contect_object_name=('seguro')
	success_url = reverse_lazy('seguros')