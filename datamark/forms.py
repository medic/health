# -*- coding: utf-8 -*-

from django.forms import Form, ModelForm, Select, Textarea, DateInput, TextInput, SelectMultiple, BooleanField
from django import forms
from django.forms.formsets import formset_factory, DELETION_FIELD_NAME

from django.forms.models import modelformset_factory, inlineformset_factory, BaseInlineFormSet

from datamark.models import *

from consts import DIAS_OPCIONES


class EstablecimientoForm(ModelForm):
    class Meta:
        exclude = ( 'doctor', )
        model=Establecimiento       
        
    def __init__(self, *args, **kwargs):
        super(EstablecimientoForm, self).__init__(*args, **kwargs)

        for campo in self.fields.itervalues():
            campo.widget.attrs['title'] = campo.help_text

        self.fields['nombre'].widget.attrs['class'] = 'span4'
        self.fields['descripcion_corta'].widget.attrs['class'] = 'span4'
        self.fields['seguros'].widget.attrs['size'] = 15


class ZonaForm(ModelForm):
    class Meta:
        model=Zona

    def __init__(self, *args, **kwargs):
        super(ZonaForm, self).__init__(*args, **kwargs)

        for campo in self.fields.itervalues():
            campo.widget.attrs['title'] = campo.help_text 
        
class DoctorForm(ModelForm):
    class Meta:
        model=Doctor

    def __init__(self, *args, **kwargs):
        super(DoctorForm, self).__init__(*args, **kwargs)

        for campo in self.fields.itervalues():
            campo.widget.attrs['title'] = campo.help_text

        self.fields['especialidades'].widget.attrs['size'] = 15
        self.fields['nombre'].widget.attrs['class'] = 'span4'
        self.fields['apellido'].widget.attrs['class'] = 'span4'        
        self.fields['presentacion'].widget.attrs['rows'] = 2
        self.fields['seguros'].widget.attrs['size'] = 15

class ServiciosDeMedioForm(Form):

    def __init__(self, medio=None, servicios_est=None, *args, **kwargs):
        super(ServiciosDeMedioForm, self).__init__(*args, **kwargs)        

        if (medio is not None):            

            servicios = Servicio_Publicitario.objects.filter(medio_publicitario=medio)
            for servicio in servicios:
                servicio_id = 'servicio_'+str(servicio.id)

                if (servicios_est is not None) and (servicio in list(servicios_est)):
                    self.fields.insert(0, 
                        servicio_id,
                        BooleanField(label=servicio.descripcion, initial=True))
                    self.fields[servicio_id].widget.attrs['class'] = 'servicios'
                else:
                    self.fields.insert(0, 
                        servicio_id,
                        BooleanField(label=servicio.descripcion, initial=False))
                    self.fields[servicio_id].widget.attrs['class'] = 'servicios'

class MedioPublicitarioForm(forms.ModelForm):
    """Formulario para un Medio Publicitario"""

    class Meta:
        model = Medio_Publicitario        

class ServicioPublicitarioForm(forms.ModelForm):
    """"Formulario para un servicio publicitario """
    class Meta:
        model = Servicio_Publicitario
        exclude = ('medio_publicitario',)

class EspecialidadForm(ModelForm):
    """"Formulario para una Especialidad """
    class Meta:
        model = Especialidad

class SeguroForm(ModelForm):
    """"Formulario para un Seguro Medico """
    
    class Meta:
        model = Seguro

    
      
    
    



# Formset para servicios publicitarios
ServicioPublicitarioFormSet = inlineformset_factory(Medio_Publicitario, Servicio_Publicitario, exclude=('medio_publicitario',), extra=2)

# Formset para un establecimiento
TelefonoEstablecimientoFormSet = inlineformset_factory(Establecimiento, Telefono_Establecimiento, exclude=('establecimiento'), extra=1)

class ConsultaFormsetBase(BaseInlineFormSet):

    def add_fields(self, form, index):
        super(BaseInlineFormSet, self).add_fields(form, index)        

        try:
            instance = self.get_queryset()[index]
            pk_value = instance.pk
        except IndexError:
            instance=None
            pk_value = form.prefix
        except TypeError:
            instance=None
            pk_value = form.prefix

        data = self.data if self.data and index is not None else None

        form.bloques = BloqueConsultaFormset(data=data,
                            instance = instance,
                            prefix = 'bloque_consulta_%s' % (pk_value))

    def is_valid(self):
        resultado = super(BaseInlineFormSet, self).is_valid()

        for form in self.forms:
            if hasattr(form, 'bloques'):
                resultado = resultado and form.bloques.is_valid()

        return resultado

    def save_new(self, form, commit=True):
        "Guarda y devuelve una nueva instancia de un modelo para el formulario"
        
        modelo = super(BaseInlineFormSet, self).save_new(form, commit=False)
        # modelo.doctor = self.instance
        # if commit:
        #     modelo.save()

        form.instance = modelo

        form.bloques.instance = modelo 

        for data in form.bloques.cleaned_data:
            data[form.bloques.fk.name] = modelo

        return modelo

    def should_delete(self, form):

        if self.can_delete:
            raw_delete_value = form._raw_value(DELETION_FIELD_NAME)
            should_delete = form.fields[DELETION_FIELD_NAME].clean(raw_delete_value)
            return should_delete

        return False

    def save_all(self, commit=True):

        modelos = self.save(commit=False)

        if commit:
            for m in modelos:
                setattr(m, self.fk.name, self.instance ) 
                m.save()

        if not commit:
            self.save_m2m()

        for form in set(self.initial_forms + self.saved_forms):
            if self.should_delete(form): continue

            form.bloques.save(commit=commit)


class BloqueFormset(BaseInlineFormSet):
    """ Clase de Formset para consultas """

    def add_fields(self, form, index):
        super(BaseInlineFormSet, self).add_fields(form, index)

        field_base = form.fields['dias']        
        form.fields['dias'].widget= forms.CheckboxSelectMultiple(choices=DIAS_OPCIONES)
        form.fields['dias'].widget.help_text= field_base.help_text       

        
# Formset para el registro de consultas de un doctor
ConsultaFormset = inlineformset_factory(Doctor, Consulta, formset=ConsultaFormsetBase,exclude=('doctor',), extra=1)

# Formset para el registro de bloques de atención de un doctor
BloqueConsultaFormset = inlineformset_factory(Consulta, Bloque_Consulta,exclude=('consulta',), formset=BloqueFormset, extra=1)

# Formset para el registro de horarios de atención de un establecimiento
BloqueAtencionFormset = inlineformset_factory(Establecimiento, Bloque_Atencion, exclude=('establecimiento',), formset=BloqueFormset, extra=0)

# Formset para el registro de teléfonos particulares de un doctor
TelefonoDoctorFormset = inlineformset_factory(Doctor, Telefono_Doctor, exclude=('doctor',), extra=1)