
from django import template 

register = template.Library()

# Tag para agregar un archivo CSS contenido en el proyecto mediante STATIC_URL

class LocalCSSNode(template.Node):
	"""Nodo para un archivo CSS local"""

	# Si el archivo se recibe sin formato .css se agrega
	def __init__(self, css_url):
		self.css_url = css_url

	def render(self, context):

		if (self.css_url[-4:] != ".css"):
			self.css_url = self.css_url + ".css"

		return ("<link rel=\"stylesheet\" href=\"%scss/%s\" >" % (context['STATIC_URL'] , self.css_url))

def local_css(parser, token):
	"""Funcion manejadora del tag para aplicar un CSS local"""

	try:
		tag, css_url = token.split_contents()
	except ValueError:
		raise template.TemplateSyntaxError("%r tag requiere al menos un argumento" % token.contents.split()[0])

	return LocalCSSNode(css_url[1:-1])

# Tag para agregar un archivo Javascript contenido en el proyecto mediante STATIC_URL

class LocalJSNode(template.Node):
	"""Nodo para un archivo Javascript local"""

	def __init__(self, js_url):
		self.js_url = js_url

	def render(self, context):

		# Si el archivo se recibe sin formato .js se agrega
		if (self.js_url[-3:] != ".js"):
			self.js_url = self.js_url + ".js"

		return ("<script src=\"%sjs/%s\" ></script>" % (context['STATIC_URL'] , self.js_url))

def local_js(parser, token):
	"""Funcion manejadora del tag para aplicar un Javascript local"""

	try:
		tag, js_url = token.split_contents()
	except ValueError:
		raise template.TemplateSyntaxError("%r tag requiere al menos un argumento" % token.contents.split()[0])

	return LocalJSNode(js_url[1:-1])

# Tag para agregar un submit de Health

def health_submit(titulo):

	wrapper= """<div class=\"submit-wrapper\">
					<div class=\"delimitador\"></div>
					<button type=\"submit\" class=\"btn btn-success btn registro-submit\">%s</button>		
					<div class=\"delimitador\"></div>
				</div> """

	return (wrapper % (titulo))

# Tag para agregar JavaScript que peudan venir de CDNS

def cdn_js(parser, token):
	"""Funcion manejadora del tag para aplicar un Javascript que pueda provenir de un CDN"""

	try:
		tag, nombre, js_url_cdn, js_url_local = token.split_contents()
	except ValueError:
		raise template.TemplateSyntaxError("%r tag requiere de tres argumentos argumentos" % token.contents.split()[0])

	return CDNJSNode(nombre[1:-1], js_url_local[1:-1], js_url_cdn[1:-1])

class CDNJSNode(template.Node):
	"""Nodo para un archivo Javascript proveniente de un CDN"""

	def __init__(self, nombre, js_url_local, js_url_cdn):
		self.js_url_local = js_url_local
		self.js_url_cdn = js_url_cdn
		self.nombre = nombre

	def render(self, context):

		# Si el archivo se recibe sin formato .js se agrega
		if (self.js_url_local[-3:] != ".js"):
			self.js_url_local = self.js_url_local + ".js"

		if (self.js_url_cdn[-3:] != ".js"):
			self.js_url_cdn = self.js_url_cdn + ".js"

		cad = """<script src=\"%s\"></script>

    <script> window.%s || document.write('<script src=\"%sjs/%s\"><\/script>')</script> """

		return (cad % (self.js_url_cdn, self.nombre, context['STATIC_URL'] , self.js_url_local))

# Registros de tags	

register.tag('local_css', local_css)
register.tag('local_js', local_js)
register.tag('cdn_js', cdn_js)
register.simple_tag(health_submit)