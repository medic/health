# -*- coding:utf-8 -*-

from django.conf import settings
from django.db import models
from django.utils.encoding import smart_unicode
from consts import *
import datetime


# Uso de JSON 
import json

from django.contrib.auth.models import User

from datamark_mdb.models import *

# Módelos abstractos

class MSESyncModel(models.Model):
	"Aspectos comunes para modelos sincronizados en el MSE"

	class Meta:
		abstract = True

	mse_id = models.CharField(max_length=25, editable=False, blank=True, 
		help_text='Id del registro en el MSE')	

	def get_mse_id(self):
		"""Devuelve el id en el MSE validando si está sincronizado o no"""

		return self.mse_id if self.mse_id != '' else None		

	def to_mse_model(self):
		"""
			Todo modelo que herede de esta clase debe implementar este 
			método. Crea el modelo equivalente en el MSE y traspasa los
			campos necesarios. Devuelve el modelo MSE. No es necesario
			traspasar el id.
		"""
		pass

	def save_mse(self, update=False, mse_commit=True):

		"""
			Guarda el modelo en la base de datos de MSE. 
			En caso de necesitarse llamar al método save_m2m(), debe llarmase
			tambien este método de forma explicita. En caso contrario es 
			suficiente con llamar a save().
		"""

		if settings.WITH_MONGODB and mse_commit:	
			mse_model = self.to_mse_model()

			mse_model.id = self.get_mse_id()

			mse_model.save()		

			if (self.mse_id == '' or update is True):			
				self.mse_id = mse_model.id
				super(MSESyncModel, self).save()			


	def save(self, update=False, mse_commit=True, *args, **kwargs):		
		super(MSESyncModel, self).save(*args, **kwargs)		

		if 'update' not in kwargs.keys():
			self.save_mse(mse_commit=mse_commit)
		else:
			self.save_mse(mse_commit=mse_commit, update=True)
		
			

	def delete(self, using=None):

		if settings.WITH_MONGODB:	
			mse_model = self.to_mse_model()
			mse_model.id = self.get_mse_id()

			mse_model.delete()

			print "Eliminado " + self.nombre
		

		super(MSESyncModel, self).delete(using=using)


class ValidModel (models.Model):
	class Meta:
		abstract = True

	validado_por = models.ForeignKey(User,editable=False,null=True,related_name="%(app_label)s_%(class)s_related")
	validado_el = models.DateTimeField(editable=False,null=True)

class RevisedModel(models.Model):

	class Meta:
		abstract = True

	revisado_por = models.ForeignKey(User, editable=False)
	revisado_el = models.DateTimeField(editable=False, default=datetime.datetime.now())	

	def save(self, *args, **kwargs):

		if not kwargs.pop('not_revision', False):		
			self.revisado_el = datetime.datetime.now()
				
		super(RevisedModel, self).save(*args, **kwargs)


# Modelos en la base de datos

class Seguro(models.Model):

	nombre = models.CharField(max_length=45,help_text='Nombre de la Empresa Aseguradora',blank=False)
	telefono = models.CharField("Teléfono", max_length=14,blank=True,help_text='Nº de Telefono.')
	direccion = models.CharField(max_length=45,help_text='Dirección de la Empresa de Seguros', blank=True)
	logo=models.ImageField(upload_to='seguros', blank=True, null=True)
	rif = models.CharField('RIF', max_length=13, 
		blank=True,
		help_text='Rif de la compañia de seguros')


	
	def __unicode__(self):
		return self.nombre


class Especialidad(MSESyncModel):
	"""Especialidades de medicina"""

	class Meta:
		verbose_name_plural=u"especialidades"
	
	especialidad = models.CharField(max_length=45,
		unique=True,
		help_text='Nombre de la especialidad.')

	nombre_especialista = models.CharField(max_length=45, blank=True, help_text='Nombre del especialista que ejerce la especialidad.')
	palabras_claves = models.TextField(blank=True, help_text='Palabras que pueden estar asociadas a la especialidad.')
	descripcion = models.TextField(blank=True, help_text='En qué consiste la especialidad.')

	def __unicode__(self):
		return self.especialidad

	def to_mse_model(self):		
		especialidad_mse = EspecialidadMSE()		

		especialidad_mse.nombre_especialista = self.nombre_especialista		
		especialidad_mse.palabras_claves = self.palabras_claves
		especialidad_mse.descripcion = self.descripcion
		especialidad_mse.especialidad = self.especialidad	

		return especialidad_mse	
		

class Doctor(MSESyncModel,ValidModel, RevisedModel):
	"""Informacion general de un doctor o especialista."""

	SEXOS = (
		(MASCULINO, "Hombre"),
		(FEMENINO, "Mujer"),
		(SIN_ESPECIFICAR, 'No especificado')
	)

	TITULOS = (
		(DOCTOR, 'Doctor'),
		(LICENCIADO, 'Licenciado'),
		(PSICOLOGO, 'Psicólogo')

	)	
	seguros =models.ManyToManyField(Seguro,blank=True, null=True, 
		help_text='Seguro asociados al establecimiento.')
	titulo = models.CharField('Título', max_length=5, choices=TITULOS, default=DOCTOR)
	nombre = models.CharField('Nombres', max_length=45,help_text='Nombre(s) del Doctor.')
	apellido = models.CharField('Apellidos', max_length=45,help_text='Apellido(s) del Doctor.')

	usuario = models.CharField(max_length=45, 
		blank=False, 
		null=False, 
		unique=True,
		help_text='Usuario del doctor en Health, será el nombre del doctor que tendrá para la URL personalizada de su perfil.')

	rif = models.CharField('RIF', max_length=13, 
		blank=True,
		help_text='Rif del doctor')

	sexo = models.CharField(max_length=2, choices=SEXOS, default=DOCTOR, help_text='Sexo del doctor.')

	presentacion = models.TextField('Presentación', blank=True, help_text='Texto adicional que identifica una característica del doctor, cómo: \"Egresado de\", \"con maestria en\", etc.')
	servicios = models.TextField(blank=True,help_text='Servicios ofrecidos por el doctor en sus consultas. Separar por comas(,).')
	especialidades = models.ManyToManyField(Especialidad,help_text='Especialidades del doctor.' )

	adicional = models.TextField('Información adicional', blank=True,
		help_text='Información adicional que nos corresponda como tal a su listado de servicios ni especialidades pero que sirve como parte de su promoción.')
	
	correo = models.EmailField(max_length=45,blank=True,help_text='Correo electronico.')
	web = models.CharField(max_length=45,blank=True, help_text='Sito Web.')	
	twitter = models.CharField(max_length=20,blank=True,help_text='Twitter personal')	
	facebook = models.CharField(max_length=20,blank=True,help_text='Página personal de Facebook.')	
	
	agregado_el = models.DateTimeField(auto_now_add=True, 
		null=False, editable=False,
		help_text='Momento en el que fue agregado el doctor al sistema'
		)

	revisado_de = models.ForeignKey('Medio_Publicitario',
		help_text='Medio del cual fue tomada la información')

	def sexo_doctor(self):
		for t in self.SEXOS:
			if self.sexo == t[0]:
				return t[1]
				
	def to_mse_model(self):
		doctor_mse             = EntityMSE()
		doctor_mse.entity_type = TIPOS_ENTIDADES['doctor']
		
		doctor_mse.nombre      = self.nombre
		doctor_mse.apellido    = self.apellido
		doctor_mse.usuario     = self.usuario
		doctor_mse.sexo        = self.sexo
		doctor_mse.servicios   = self.servicios
		doctor_mse.correo      = self.correo
		doctor_mse.web         = self.web
		doctor_mse.twitter     = self.twitter
		doctor_mse.titulo      = self.titulo

		telefonos = []

		for t in self.telefono_doctor_set.all():
			telefonos.append(t.telefono)

		doctor_mse.telefonos.extend(telefonos)

		especialidades = []				

		for e in self.especialidades.all():
			especialidades.append(EspecialidadMSE(
				especialidad=e.especialidad,
				nombre_especialista=e.nombre_especialista,
				palabras_claves=e.palabras_claves
				))		

		doctor_mse.especialidades.extend(especialidades)

		return doctor_mse

	def from_mse_model(self, mse_model):

		if mse_model.entity_type == TIPOS_ENTIDADES['doctor']:
		
			self.mse_id      = mse_model.id
			self.nombre      = mse_model.nombre
			self.apellido    = mse_model.apellido
			self.usuario     = mse_model.usuario
			self.sexo        = mse_model.sexo
			self.servicios   = mse_model.servicios
			self.correo      = mse_model.correo
			self.web         = mse_model.web
			self.twitter     = mse_model.twitter
			self.titulo      = mse_model.titulo

			self.revisado_por_id = 4
			self.revisado_el = datetime.datetime.now()
			self.revisado_de_id = 2
			

	def related_data_from_mse_model(self, mse_model):
		for t in mse_model.telefonos:			 	
		 	self.telefono_doctor_set.add(Telefono_Doctor(telefono=t))						

		for e in mse_model.especialidades:
			especialidad = Especialidad.objects.get(especialidad=e.especialidad)
			self.especialidades.add(especialidad)

	def servicios_readable	(self):
		return self.servicios.split(',')	

	def has_contacts(self):
		if self.twitter or self.facebook or self.correo or self.web or self.telefono_doctor_set.count():
			return True
		return False

	def __unicode__(self):
		return self.nombre + self.apellido

class Telefono_Doctor(models.Model):
	"""Teléfonos de doctores"""

	telefono = models.CharField('Teléfono', max_length=14,blank=True,help_text='Nº de Telefono particular del doctor.')
	doctor = models.ForeignKey(Doctor)

	def __unicode__(self):
		return self.telefono

class Medio_Publicitario(models.Model):
	"""Informacion de los diferentes medios publicitarios usados por doctores y centros de salud."""

	class Meta:
		verbose_name=u'Medio publicitario'
		verbose_name_plural=u'medios publicitarios'

	TIPO = (
		(REVISTA,  'Revista'),
		(GUIA_MEDICA,  'Guia Medica'),
		(GUIA_MEDICA_WEB, 'Guia Medica Web'),
		(PUBLICIDAD_WEB, 'Publicidad Web'),		
		(PUBLICIDAD_WEB, 'Otro'),	
	)

	tipo = models.CharField(max_length=2,choices=TIPO, help_text='Tipo de publicidad.')
	nombre = models.CharField(max_length=45,help_text='Nombre de la empresa que lo ofrece.')
	rif = models.CharField(max_length=13, blank=True, help_text='RIF del medio publicitario')
	descripcion = models.TextField(max_length=45,blank=True,help_text='¿En que consiste?')	
	telefono = models.CharField(max_length=11,blank=True,help_text='Nº de Telefono.')
	correo = models.EmailField(max_length=45,blank=True, help_text='Correo electrónico.')
	twitter = models.CharField(max_length=20,blank=True,help_text='Cuenta de Twitter (sin el @)')
	web = models.CharField(max_length=45,blank=True,help_text='Sitio Web.')

	def nombre_tipo(self):
		for t in self.TIPO:
			if self.tipo == t[0]:
				return t[1]

	def __unicode__(self):
		return self.nombre + "(" + self.nombre_tipo() + ")"

class Servicio_Publicitario(models.Model):
	"""Servicio de publicidad que ofrece un medio publicitario."""

	class Meta:
		verbose_name=u'Servicio publicitario'
		verbose_name_plural= u'servicios publicitarios'

	descripcion = models.CharField(max_length=45,help_text='Detalles del servicio.')
	costo = models.DecimalField(max_digits=8, default=0, null=True, decimal_places=2,help_text='Costo del servicio. Puede colocarse un estimado.')
	costo_real =  models.BooleanField(help_text='Indica si el costo especificado es un supuesto o si ha sido investigado.',
		verbose_name=u'¿Es el costo real? ', default=False)
	medio_publicitario = models.ForeignKey(Medio_Publicitario,help_text='Empresa que ofrece el servicio.')

	def __unicode__(self):
		return self.descripcion


class Ciudad(models.Model):
	"""Ciudad en la base de datos"""

	class Meta:
		verbose_name_plural=u'Ciudades'

	descripcion = models.CharField(max_length=45,help_text='Nombre de la ciudad.')
	codigo_area = models.CharField(max_length=4, blank=True, help_text='Código de area telefónico de la ciudad.P')

	def __unicode__(self):
		return self.descripcion

class Zona(models.Model):
	"""Parroquia o Zona de Atencion"""
	descripcion = models.CharField(max_length=45,help_text='Nombre de la zona.')
	ciudad = models.ForeignKey(Ciudad,help_text='Ciudad a la que pertenece.')

	def __unicode__(self):
		return self.descripcion

class Establecimiento(MSESyncModel, ValidModel,RevisedModel):

	"""Locales de salud"""

	TIPOS_ESTABLECIMIENTO =	(
		('CL','Clínica'),
		('FA','Farmacia'),
		('GY','Gimnasio'),
		('CI','Consultorio Independiente'),
		('LB', 'Laboratorio clínico'),
		('CS', 'Centro de salud'),
		('CE', 'Centro estético'),		
		('OP', 'Óptica'),
		('EM', 'Insumos y equipos médicos'),
		('VT', 'Consultorio veterinario'),
		('CV', 'Clínica veterinaria'),
		('OT', 'Otros')
	)

	
	nombre = models.CharField(max_length=100, 
		help_text='Nombre de establecimiento')

	descripcion_corta = models.CharField(max_length=80,
		blank=True,
		help_text='Texto corto que a veces acompaña al nombre de un establecimiento y describe que es, como:Instituto Odontológico, Servicio Odontológico, Unidad de Medicina, etc.'
		)

	adicional = models.TextField("Información adicional", blank=True,
		help_text='Información adicional que nos corresponda como tal a su listado de servicios.')

	usuario = models.CharField(max_length=45, 
		blank=False, 
		unique=True,
		help_text='Usuario para un establecimiento. A partir de este nombre se formará su URL')

	presentacion = models.TextField('Presentación/Eslogan',max_length=140,
		blank=True,
		help_text='Texto que caracteriza al establecimiento. Como: Clínica con mas de 50 años de experiencia. '
		)

	rif = models.CharField(max_length=13, 
		blank=True,
		help_text='Rif del establecimiento')	

	direccion = models.TextField('Dirección', blank=False,
		help_text='Direccion del Establecimiento.')

	tipo = models.CharField(max_length=2,		
		choices=TIPOS_ESTABLECIMIENTO, 		
		help_text='Tipo de negocio.')

	fecha_inauguracion = models.DateField( 'Fecha de inauguración', 
		blank=True, 
		null=True,
		help_text='Fecha en el que fue inagurado el local.')

	correo = models.EmailField(max_length=45,		
		blank=True,
		help_text='Correo de cotacto.')

	seguros = models.ManyToManyField(Seguro,blank=True, null=True, 
		help_text='Seguros con los que trabaja el Establecimiento.')
	

	servicios = models.TextField(
		blank=True,
		help_text='Servicios que ofrece el negocio. Separar cada servicio por comas (,).')

	todo_momento =  models.BooleanField(verbose_name=u'Disponible las 24 horas', 
		help_text='Especifica si funciona las 24 horas, es decir, no posee horario como tal, como el caso de algunas farmacias')
	emergencias = models.BooleanField(verbose_name=u'Emergencia las 24 horas', 
		help_text='En el caso de ser una clínica o parecido, si atiende emergencias a toda hora.')

	web = models.CharField(max_length=60,
		blank=True,
		help_text='Sitio Web de local')

	twitter = models.CharField(max_length=15,		
		blank=True,
		help_text='Cuenta de Twitter.')

	facebook = models.CharField(max_length=20,
		blank=True,
		help_text='Página de Facebook.')	

	ciudad = models.ForeignKey(Ciudad, 
		null=False,
		help_text='Ciudad en la que se encuentra el establecimiento.')

	zona = models.ForeignKey(Zona,
		null=True, 
		blank=True,
		help_text='Zona de la ciudad en la que se encuentra el negocio.')

	servicios_publicitarios = models.ManyToManyField(Servicio_Publicitario,
		blank=True,
		help_text='Servicios publicitarios usados por el establecimiento.')

	doctores = models.ManyToManyField(Doctor, editable=False,
		through='Consulta')

	agregado_el = models.DateTimeField(auto_now_add=True, 
		editable=False,
		help_text='Fecha en que fue agregado el establecimiento en la base de datos')

	revisado_de = models.ForeignKey('Medio_Publicitario', default=None,
		help_text='Medio del cual fue tomada la información')

	def to_mse_model(self):		
		establecimiento_mse                   = EntityMSE()		
		establecimiento_mse.entity_type       = TIPOS_ENTIDADES['establecimiento']
		
		establecimiento_mse.nombre            = self.nombre		
		establecimiento_mse.usuario           = self.usuario
		establecimiento_mse.servicios         = self.servicios
		establecimiento_mse.direccion         = self.direccion
		establecimiento_mse.tipo_establecimiento              = self.nombre_tipo()
		establecimiento_mse.descripcion_corta = self.descripcion_corta
		establecimiento_mse.web               = self.web
		establecimiento_mse.twitter           = self.twitter
		establecimiento_mse.facebook          = self.facebook
		establecimiento_mse.correo            = self.correo
		establecimiento_mse.todo_momento      = self.todo_momento
		establecimiento_mse.emergencias       = self.emergencias

		telefonos = []

		for t in self.telefono_establecimiento_set.all():
			telefonos.append(t.telefono)

		establecimiento_mse.telefonos.extend(telefonos)

		return establecimiento_mse 		

	def from_mse_model(self, mse_model):

		if mse_model.entity_type == TIPOS_ENTIDADES['establecimiento']:
		
			self.nombre            = mse_model.nombre		
			self.usuario           = mse_model.usuario
			self.servicios         = mse_model.servicios
			self.direccion         = mse_model.direccion

			if mse_model.tipo_establecimiento:

				self.tipo              = self.tipo_por_nombre(mse_model.tipo_establecimiento)
			else:
				self.tipo = 'CL'

			self.descripcion_corta = mse_model.descripcion_corta
			self.web               = mse_model.web
			self.twitter           = mse_model.twitter
			self.facebook          = mse_model.facebook
			self.correo            = mse_model.correo
			self.todo_momento      = mse_model.todo_momento
			self.emergencias       = mse_model.emergencias
			self.ciudad_id = 1

			self.revisado_por_id = 4
			self.revisado_el = datetime.datetime.now()
			self.revisado_de_id = 2
		

	def related_data_from_mse_model(self, mse_model):
		for t in mse_model.telefonos:			 	
		 	self.telefono_establecimiento_set.add(Telefono_Establecimiento(telefono=t))						

		

	def nombre_tipo(self):
		for t in self.TIPOS_ESTABLECIMIENTO:
			if self.tipo == t[0]:
				return t[1]

	def tipo_por_nombre(self, nombre):
		for t in self.TIPOS_ESTABLECIMIENTO:
			if nombre == t[1]:
				return t[0]

	def __unicode__(self):
		return  "%s (%s) " % (self.nombre , smart_unicode(self.nombre_tipo()) )		
	
class Consulta(models.Model):	
	"""Detalles la consulta de un doctor"""

	doctor = models.ForeignKey(Doctor,help_text='Doctor que realiza la consulta')

	establecimiento = models.ForeignKey(Establecimiento, 
		blank=True, null=True, 
		help_text='Lugar donde se realiza la consulta.')

	detalle_sitio = models.TextField(
		blank=True,
		help_text='Detalles del sitio del consultorio (Piso, n° de consultorio, etc.) ')

	telefono_consultorio = models.CharField(max_length=11,blank=True,help_text='Nº de Telefono .')
	previa_cita = models.BooleanField(default=False, help_text='Indica si el debe consertarse una cita antes de la consulta.')
	extension_telefono = models.CharField('Extensión del consultorio', max_length=4,blank=True,help_text='Nº de Telefono.')

	def __unicode__(self):
		return "Dr. %s %s " % (doctor.nomre, doctor.apellido)		

class Telefono_Establecimiento(models.Model):
	"""Numeros telefonicos de locales."""

	telefono = models.CharField("Teléfono", max_length=14,blank=True,help_text='Nº de Telefono.')	
	establecimiento = models.ForeignKey(Establecimiento)

	def __unicode__(self):
		return self.establecimiento + "telefono:"+ self.telfono

class Bloque_Atencion(models.Model):
	"""
		Especifica dia e intervalo de tiempo en 
		que un establecimiento ofrece servicios
	"""	

	dias = models.CharField('Días', max_length=32, 		
		help_text="Dia(s) en que se atiende con el bloque de hora especificado.")

	manana_hora_inicio = models.TimeField('Desde las', 
		null=True,
		blank=True, 
		default=None, 
		help_text='Hora en que inicia el bloque te atención en la mañana.')

	manana_hora_fin = models.TimeField('Hasta las', 
		null=True, 
		blank=True,
		default=None, 
		help_text='Hora en que finaliza el bloque de atención en la mañana.')	

	tarde_hora_inicio = models.TimeField('Desde las', 
		null=True, 
		blank=True,
		default=None,
		help_text='Hora en que finaliza el bloque de atención en la mañana.')	

	tarde_hora_fin = models.TimeField('Hasta las', 
		null=True, 
		blank=True,
		default=None,
		help_text='Hora en que finaliza el bloque de atención en la mañana.')	

	establecimiento = models.ForeignKey(Establecimiento)
	

class Bloque_Consulta(models.Model):
	""" 
		Especifica el dia y el intervalo de tiempo en que un doctor
		realiza consultas
	"""	

	DIAS= ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo','Lunes a Viernes','Todos los dias']

	emergencias = models.BooleanField('Horario para emergencias',default=False, help_text='Si el horario especificado es para emergencias')
	
	dias = models.CharField(max_length=32, verbose_name=u'Días',			
		help_text='Dia(s) en los que se realia la consulta')

	manana_hora_inicio = models.TimeField('Desde las',
		null=True, blank=True,
		default=None,		
		help_text='Hora en que inicia el bloque de consultas en la mañana.')

	manana_hora_fin = models.TimeField('Hasta las',null=True, 
		default=None, blank=True,
		help_text='Hora en que finaliza el bloque de consultas en la mañana.')

	tarde_hora_inicio = models.TimeField('Desde las', 
		null=True, blank=True,
		default=None,
		help_text='Hora en que inicia el bloque de consultas en la tarde.')

	tarde_hora_fin = models.TimeField('Hasta las', 
		null=True, blank=True,
		default=None,
		help_text='Hora en que finaliza el bloque de consultas en la tarde.')

	consulta = models.ForeignKey(Consulta)

	def nombre_dia(self):
		dias = []
		for elemento in eval(self.dias):
			pos = int(elemento)-1
			dias.append(self.DIAS[pos])
		return dias

	def dias_readable(self):
		seleccion =  sorted(eval(self.dias))
		salida = ''
		if len(seleccion) <= 1:
			return self.DIAS[int(seleccion[0])-1]
		elif len(seleccion)<=2: 
			if '8' in seleccion:
				return self.DIAS[int(seleccion[1])-1] + ' y ' + self.DIAS[int(seleccion[0])-1]
			return self.DIAS[int(seleccion[0])-1] + ' y ' + self.DIAS[int(seleccion[1])-1]
		else: 
			
			elementos=[]
			for item in seleccion:
				if seleccion.index(item)<len(seleccion)-1:
					if int(item) == int(seleccion[seleccion.index(item)+1]) - 1:
						elementos.append(item)
						if seleccion.index(item) == len(seleccion)-1:
							elementos.append(seleccion[len(seleccion)-1])
							break
					elif len(elementos)>=2:
						if salida=='':
							salida+='de '+ self.DIAS[int(elementos[0])-1] + ' a ' + self.DIAS[len(elementos)] 
							elementos=[]
						else: 
							salida+=', y de '+ self.DIAS[int(elementos[0])-1] + ' a ' + self.DIAS[len(elementos)] 
							elementos=[]

					elif elementos:
						for e in elementos:
							salida+= ', ' + self.DIAS[int(e)-1]
						salida += ', ' + self.DIAS[int(item)-1]
						elementos=[]
					else:
						salida += ', ' + self.DIAS[int(item)-1]
				elif len(elementos)>=2:
					if salida=='':
						salida+='de '+ self.DIAS[int(elementos[0])-1] + ' a ' + self.DIAS[int(item)-1] 
						elementos=[]
					else: 
						salida+=' y de '+ self.DIAS[int(elementos[0])-1] + ' a ' + self.DIAS[int(item)-1] 
						elementos=[]
				else:
					salida += ' y ' + self.DIAS[int(item)-1]



		return salida 

	def horas_manana_readable(self):

		if self.manana_hora_inicio and self.manana_hora_fin:
			return 'de '+str(self.manana_hora_inicio)+' a '+str(self.manana_hora_fin)
		elif self.manana_hora_inicio and not self.manana_hora_fin:
			return 'a partir de la(s) '+str(self.manana_hora_inicio)

		return self.manana_hora_inicio
	
	def horas_tarde_readable(self):
		if (self.tarde_hora_inicio and not self.tarde_hora_fin) :
			return 'a partir de la(s) '+str(self.tarde_hora_inicio)
		elif (self.tarde_hora_inicio and not self.tarde_hora_fin):
			return 'de '+str(self.tarde_hora_inicio)+' a '+str(self.tarde_hora_fin)
		return None
		
	def __unicode__(self):
		return self.dias + str(self.manana_hora_inicio)