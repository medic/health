# -*- coding: utf-8 -*-

# Constantes para Datamark


# Tipos de medios publicitarios

REVISTA = 'R'
GUIA_MEDICA = 'G'
GUIA_MEDICA_WEB = 'GW'
PUBLICIDAD_WEB = 'PW'

# Sexos

MASCULINO = 'M'
FEMENINO = 'F'
SIN_ESPECIFICAR = 'N'

# Título para un especialista

DOCTOR = "Dr"
LICENCIADO = 'Lic'
PSICOLOGO = 'Psic'

# Dias de la semana

LUNES = '1'
MARTES = '2'
MIERCOLES = '3'
JUEVES = '4'
VIERNES = '5'
LUNES_A_VIERNES = '8'
SABADO = '6'
DOMINGO = '7'
TODOS_LOS_DIAS = '9'

DIAS_OPCIONES = (		
		(LUNES, 'Lunes'),
		(MARTES, 'Martes'),
		(MIERCOLES, 'Miércoles'),
		(JUEVES, 'Jueves'),
		(VIERNES, 'Viernes'),		
		(SABADO, 'Sábado'),
		(DOMINGO, 'Domingo'),
		(LUNES_A_VIERNES, 'Lunes a Viernes'),		
		(TODOS_LOS_DIAS, 'Todos los días'),		
	)

# Tipos de entidades

TIPOS_ENTIDADES = {
	'doctor': 'D',
	'establecimiento': 'E'
}

