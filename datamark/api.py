from django.contrib.auth.models import User
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from datamark.models import Especialidad, Establecimiento, Doctor, Servicio_Publicitario, Telefono_Doctor

class EspecialidadResource(ModelResource):
    class Meta:
        queryset = Especialidad.objects.all()
        resource_name = 'especialidad'
        authorization= Authorization()

class DoctorResource(ModelResource):
    revisado_de = fields.CharField(attribute="revisado_de")
    especialidad = fields.ToManyField(EspecialidadResource, 'especialidades', full=True)
#    telefonos = fields.ToOneField(TelefonoResource, 'telefonos', full=True)
    class Meta:
        queryset = Doctor.objects.all()
        resource_name = 'tdoctor'
        authorization= Authorization()

class TelefonoResource(ModelResource):
    doctor = fields.ToOneField(DoctorResource, 'doctor', full=True)
    class Meta:
        queryset = Telefono_Doctor.objects.all()
        resource_name = 'telefono'
        authorization= Authorization()

class ServicioResource(ModelResource):
    class Meta:
        queryset = Servicio_Publicitario.objects.all()
        resource_name = 'servicio'
        authorization= Authorization()

class EstablecimientoResource(ModelResource):
    zona = fields.CharField(attribute="zona")
    ciudad = fields.CharField(attribute="ciudad")
    doctores = fields.ToManyField(DoctorResource, 'doctores', full=True)
    servicios_publicitarios = fields.ToManyField(ServicioResource, 'servicios_publicitarios', full=True)
    class Meta:
        queryset = Establecimiento.objects.all()
        resource_name = 'establecimiento'
        authorization= Authorization()