from models import *
from django.contrib import admin

class EstablecimientoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'direccion')
	list_filter = ('tipo','zona')
	search_fields = ['nombre', 'servicios']

class ZonaAdmin(admin.ModelAdmin):
	list_display = ('descripcion',)
	list_filter = ('ciudad',)
	search_fields = ['descripcion']

class CiudadAdmin(admin.ModelAdmin):
	list_display  = ('descripcion',)
	search_fields = ['descripcion']	

class EspecialidadAdmin(admin.ModelAdmin):
	list_display = ('especialidad',)
	search_fields = ['especialidad']

class MedioPublicitarioAdmin(admin.ModelAdmin):
	list_display = ('nombre','descripcion', 'telefono', 'correo')
	search_fields = ('telefono',)
	fields = ('tipo', 'nombre','descripcion','telefono', 'correo','twitter','web')	

class ServicioPublicitarioAdmin(admin.ModelAdmin):
	list_display =('descripcion', 'costo', 'costo_real', 'medio_publicitario')


admin.site.register(Establecimiento, EstablecimientoAdmin)
admin.site.register(Zona, ZonaAdmin)
admin.site.register(Ciudad, CiudadAdmin)
admin.site.register(Especialidad, EspecialidadAdmin)
admin.site.register(Medio_Publicitario, MedioPublicitarioAdmin)
admin.site.register(Servicio_Publicitario,ServicioPublicitarioAdmin)