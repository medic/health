# -*- coding:utf-8 -*-

from django.db import models
from datamark.consts import *

from djangotoolbox.fields import ListField, EmbeddedModelField


class EstablecimientoMSE(models.Model):
	class Meta:
		db_table = 'mse_establecimientos'

	nombre = models.CharField(max_length=100, help_text='Nombre de establecimiento')
	usuario = models.CharField(max_length=45)
	tipo = models.CharField(max_length=25)
	servicios = models.TextField()
	direccion = models.TextField()
	descripcion_corta = models.CharField(max_length=80,
		blank=True,
		help_text='Texto corto que a veces acompaña al nombre de un establecimiento y describe que es, como:Instituto Odontológico, Servicio Odontológico, Unidad de Medicina, etc.'
		)

	web = models.CharField(max_length=60,
		blank=True,
		help_text='Sitio Web de local')

	twitter = models.CharField(max_length=15,		
		blank=True,
		help_text='Cuenta de Twitter.')

	facebook = models.CharField(max_length=20,
		blank=True,
		help_text='Página de Facebook.')

	correo = models.EmailField(max_length=45,		
		blank=True,
		help_text='Correo de cotacto.')

	telefonos = ListField()

	todo_momento =  models.BooleanField(verbose_name=u'Disponible las 24 horas', 
		help_text='Especifica si funciona las 24 horas, es decir, no posee horario como tal, como el caso de algunas farmacias')
	
	emergencias = models.BooleanField(verbose_name=u'Emergencia las 24 horas', 
		help_text='En el caso de ser una clínica o parecido, si atiende emergencias a toda hora.')

class EspecialidadMSE(models.Model):
	class Meta:
		db_table = 'mse_especialidades'

	especialidad = models.CharField(max_length=45,		
		help_text='Nombre de la especialidad.')

	nombre_especialista = models.CharField(max_length=45, blank=True, help_text='Nombre del especialista que ejerce la especialidad.')
	palabras_claves = models.TextField(blank=True, help_text='Palabras que pueden estar asociadas a la especialidad.')


class DoctorMSE(models.Model):
	class Meta:
		db_table = 'mse_doctores'	

	SEXOS = (
		(MASCULINO, "Hombre"),
		(FEMENINO, "Mujer"),
		(SIN_ESPECIFICAR, 'No especificado')
	)

	nombre = models.CharField(max_length=45,help_text='Nombre(s) del Doctor.')
	apellido = models.CharField(max_length=45,help_text='Apellido(s) del Doctor.')
	usuario = models.CharField(max_length=45, 
		blank=False, 
		null=False, 
		unique=True,
		help_text='Usuario del doctor en Health, será el nombre del doctor que tendrá para la URL personalizada de su perfil.')

	sexo = models.CharField(max_length=2, choices=SEXOS, default=DOCTOR, help_text='Sexo del doctor.')
	servicios = models.TextField(blank=True,help_text='Servicios ofrecidos por el doctor en sus consultas. Separar por comas(,).')
	correo = models.EmailField(max_length=45,blank=True,help_text='Correo electronico.')
	web = models.CharField(max_length=45,blank=True, help_text='Sito Web.')	
	twitter = models.CharField(max_length=20,blank=True,help_text='Twitter personal')	

	titulo = models.CharField('Título', max_length=2)

	# Telefonos y listado de especialidades 
	telefonos = ListField()
	especialidades = ListField(EmbeddedModelField('EspecialidadMSE'))

class EntityMSE(models.Model):
	class Meta:
		db_table = 'entities'

	# Campos comunes

	entity_type = models.CharField(max_length=1)
	nombre = models.CharField(help_text='Nombre de establecimiento o Doctor')
	usuario = models.CharField(max_length=45, 
		blank=False, 
		null=False, 
		unique=True,
		help_text='Usuario del doctor en Health, será el nombre del doctor que tendrá para la URL personalizada de su perfil.')

	telefonos = ListField()
	servicios = models.TextField()
	servicios_array = ListField()

	web = models.CharField(max_length=60,
		blank=True,
		help_text='Sitio Web de local')

	twitter = models.CharField(max_length=15,		
		blank=True,
		help_text='Cuenta de Twitter.')	

	facebook = models.CharField(max_length=20,
		blank=True,
		help_text='Página de Facebook.')

	correo = models.EmailField(max_length=45,		
		blank=True,
		help_text='Correo de cotacto.')	

	# Campos para doctores

	SEXOS = (
		(MASCULINO, "Hombre"),
		(FEMENINO, "Mujer"),
		(SIN_ESPECIFICAR, 'No especificado')
	)	
	
	apellido = models.CharField(max_length=45,help_text='Apellido(s) del Doctor.')

	sexo = models.CharField(max_length=2, help_text='Sexo del doctor.')	

	titulo = models.CharField('Título', max_length=2)
	
	especialidades = ListField(EmbeddedModelField('EspecialidadMSE'))

	# Campos para establecimientos
	
	tipo_establecimiento = models.CharField(max_length=25)	
	direccion = models.TextField()
	descripcion_corta = models.CharField(max_length=80,
		blank=True,
		help_text='Texto corto que a veces acompaña al nombre de un establecimiento y describe que es, como:Instituto Odontológico, Servicio Odontológico, Unidad de Medicina, etc.'
		)
	
	todo_momento =  models.BooleanField(verbose_name=u'Disponible las 24 horas', 
		help_text='Especifica si funciona las 24 horas, es decir, no posee horario como tal, como el caso de algunas farmacias')
	
	emergencias = models.BooleanField(verbose_name=u'Emergencia las 24 horas', 
		help_text='En el caso de ser una clínica o parecido, si atiende emergencias a toda hora.')

	afiliado = models.BooleanField(default=False)



	



