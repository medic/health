

class MSERouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    def db_for_read(self, model, **hints):
        """
        Ruteo en caso de lectura 
        """
        if model._meta.app_label == 'datamark_mdb':
            return 'mse_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Ruteo en caso de escritura
        """
        if model._meta.app_label == 'datamark_mdb':
            return 'mse_db'
        return None
    