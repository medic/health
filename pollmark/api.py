from tastypie.resources import ModelResource
from pollmark.models import *
from tastypie.authorization import Authorization


class ComentarioResource(ModelResource):
    class Meta:
        queryset = Comentario.objects.all()
        resource_name = 'comentario'
        authorization = Authorization()