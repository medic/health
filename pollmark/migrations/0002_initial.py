# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Comentario'
        db.create_table(u'pollmark_comentario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('detalle_sitio', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('conformidad', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('comentario', self.gf('django.db.models.fields.TextField')()),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('revisado', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'pollmark', ['Comentario'])


    def backwards(self, orm):
        # Deleting model 'Comentario'
        db.delete_table(u'pollmark_comentario')


    models = {
        u'pollmark.comentario': {
            'Meta': {'object_name': 'Comentario'},
            'comentario': ('django.db.models.fields.TextField', [], {}),
            'conformidad': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'detalle_sitio': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'revisado': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['pollmark']