#encoding:utf-8 

from django.conf import settings
from django.db import models

# Modelos para Pollmark


class Comentario(models.Model):
	CONFORMIDAD = (
					("SI","No encontre lo que buscaba"),
					("NO","Si encontre lo que buscaba"),
					) 
	DETALLES_SITIO = (
				("1","Bajo"),
				("2","Medio"),
				("3","Alto"),
				)
	detalle_sitio = models.CharField("¿Te gustó el potal de MEDIC?",
		choices=DETALLES_SITIO, 
		max_length=2)
	conformidad = models.CharField("¿Encontró lo que buscaba?",
		choices=CONFORMIDAD, 
		max_length=2,
		help_text='Conformidad con la busqueda')
	comentario = models.TextField("Indique su opinion",
		help_text='Su opinion es importante.')
	fecha = models.DateTimeField(auto_now=True, editable=False)
	revisado = models.BooleanField()
	session = models.TextField("ID Session")


	def get_conformidad(self):
		for t in self.CONFORMIDAD:
			if self.conformidad == t[0]:
				return t[1]

	def get_detalle_sitio(self):
		for t in self.DETALLES_SITIO:
			if self.detalle_sitio == t[0]:
				return t[1]


	def __unicode__(self):
		return str(self.fecha) + str(self.revisado)