#encoding:utf-8
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.conf import settings

from models import *
from forms import *
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def inicio(request):
	n_conformes = Comentario.objects.filter(conformidad='CO').count()
	n_inconformes = Comentario.objects.filter(conformidad='IN').count()

	comentarios = Comentario.objects.filter(revisado=False)
	paginator = Paginator(comentarios, 5)
	comentario  = ComentarioForm()

	page = request.GET.get('page')
	try:
		elementos = paginator.page(page)
	except PageNotAnInteger:
        # If page is not an integer, deliver first page.
		elementos = paginator.page(1)
	except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
		elementos = paginator.page(paginator.num_pages)



	return render_to_response('pm-inicio.html',
		{'n_conformes': n_conformes,
		'n_inconformes': n_inconformes,
		'comentarios':comentarios,
		'formulario':comentario,
		'elementos': elementos
		},
		context_instance=RequestContext(request))

def leidos(request):
	comentarios = Comentario.objects.filter(revisado=True)
	return render_to_response('pm-inicio.html',
		{
		'elementos':comentarios,
		},
		context_instance=RequestContext(request))

def comentario(request, id_elemento):
	dato 	=	get_object_or_404(Comentario,pk=id_elemento)
	if dato.revisaso != True:
		dato.revisado = True
	return render_to_response('comentario.html',
		{'comentario':dato},
		context_instance=RequestContext(request))



def revisado(request, id_elemento=None, estado=None):
	consulta = Comentario.objects.get(pk=id_elemento)
	if int(estado):
		consulta.revisado = True
	else:
		consulta.revisado = False
	consulta.save()
	return HttpResponse(estado)

