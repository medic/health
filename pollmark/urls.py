#encoding:utf-8
from django.conf.urls import patterns, url
from django.conf.urls.defaults import *
from pollmark import views
from pollmark.api import ComentarioResource

comentario_resource = ComentarioResource()
# Definición de URLs para Pollmark

urlpatterns = patterns('',
    url(r'^$', views.inicio ),
    url(r'^/comentario/(?P<id_elemento>\d+)/(?P<estado>\d+)',views.revisado),
    url(r'^/comentario/(?P<id_elemento>\d+)',views.comentario),
    url(r'^/leidos/',views.leidos),
    url(r'^/api/', include(comentario_resource.urls)),    
)
