"""Formularios para base"""

from django.forms import ModelForm, Select, RadioSelect
from django.forms.widgets import TextInput
from django import forms
from django.http import QueryDict
from models import GananciaMovimiento, Integrante, Deposito, Cita

from django.utils.encoding import smart_unicode

def integrantes_equipo(user):
        return Integrante.objects.all()

class MovimientoGananciaForm(ModelForm):                
        
        def __init__(self,data,*args,**kwargs):

                if (isinstance(data, QueryDict)):
                        super(MovimientoGananciaForm, self).__init__(data, *args,**kwargs)
                else:
                        super(MovimientoGananciaForm,self).__init__(*args,**kwargs)
                        if ( not data.is_superuser ):
                                self.fields["integrante"] = forms.ModelChoiceField(queryset=Integrante.objects.filter(equipo=data.integrante.equipo).exclude(user_id=data.id))
                        else:
                                self.fields["integrante"] = forms.ModelChoiceField(queryset=Integrante.objects.all().exclude(user_id=data.id))

        class Meta:

                CAMBIOS = (
                        ('0.125', '0.125'),
                        ('0.25', '0.25'),
                        ('0.375', '0.375'),
                        ('0.5', '0.5'),
                        ('0.625', '0.625'),
                        ('0.75', '0.75'),
                        ('0.875', '0.875'),
                        ('1.0', '1.0'),
                )

                TIPOS = (
                        ('1' , 'Incremento'),
                        ('0' , 'Decremento'),
                )

                model = GananciaMovimiento
                exclude = ('responsable',)

                widgets = {
                        'cambio': Select(choices=CAMBIOS),
                        'tipo' : RadioSelect(choices=TIPOS)
                }
                
                fields = ('integrante', 'cambio', 'tipo', 'motivo')


class DepositoForm(forms.ModelForm):

        monto = forms.CharField(widget=TextInput(attrs={'class':'span1'}),
                help_text='Monto aportado.')

        class Meta:
                model = Deposito
                exclude = ('integrante','revisado')
    
class CitaForm(forms.ModelForm):

        descripcion_autor = forms.CharField(
                required=False,
                label='El autor es o fue',
                help_text='Especifica cosas como \"Autor de \", \"Fundador de\", \"CEO de\", etc')      

        class Meta:
                model = Cita
                exclude = ('enviado_por')