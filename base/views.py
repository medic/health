#encoding:utf-8

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.conf import settings
from django.http import HttpResponse

from django.contrib.auth import login, authenticate, logout 
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required

from models import *
from forms import *

from datetime import date
import random

# Vistas para Base

def acceso(request):
	"""Formulario de logeo y procesamiento del mismo"""	

	if request.user.is_anonymous():				
		if request.method == 'POST':								
			auth_form = AuthenticationForm(data=request.POST);			
			if auth_form.is_valid():					
				usuario = request.POST['username']
				clave = request.POST['password']
				acceso = authenticate(username=usuario, password=clave)

				if acceso is not None:
					login( request, acceso)
					return redirect('base.views.inicio')
				else:
					return HttpResponse("<h1>El authenticate no esta funcionando</h1>")
			else:
				return render_to_response('login.html', {'auth_form': auth_form}, context_instance=RequestContext(request))		
		else:
			auth_form = AuthenticationForm();
		return render_to_response('login.html', {'auth_form': auth_form}, context_instance=RequestContext(request))		
	else:
		return redirect('base.views.inicio')

@login_required
def cerrar(request):
	"""Cierre de la seción del usuario"""

	logout(request)
	return redirect('base.views.acceso')

@login_required
def inicio(request):
	"""Pantalla de inicio para todo usuario logeado."""

	# Extraccion de pensamiento del día

	cita_hoy = None

	try: 
		cita_hoy = Cita.objects.get(ultima_vez=date.today())
	except Cita.DoesNotExist:
		random.seed()
		n_citas = Cita.objects.count()
		if n_citas != 0:		
			n = random.randint(0,n_citas-1)
			n = n + date.today().day
			cita_hoy = Cita.objects.all()[(n % n_citas)]
			cita_hoy.ultima_vez = date.today()
			cita_hoy.veces_usado = cita_hoy.veces_usado + 1
			cita_hoy.save()
	
	return render_to_response('inicio_health.html', 
		{'cita': cita_hoy}, 
		context_instance=RequestContext(request))

@login_required
def registrar_cita(request):

	if request.method == 'POST':
		cita_form = CitaForm(request.POST)
		if cita_form.is_valid():
			cita = cita_form.save(commit=False)
			if cita_form.cleaned_data['descripcion_autor'] != "":
				cita.autor = cita.autor + ", " + cita_form.cleaned_data['descripcion_autor'] 
			cita.enviado_por_id = request.user.id
			cita.save()
			return redirect('base.views.inicio')	
	else:	
		cita_form = CitaForm()

	return render_to_response('registrar_cita.html', 
		{'cita_form': cita_form},
		context_instance=RequestContext(request))
		
@login_required
def cambio_ganancia(request):
	"""Formulario para realizar un cambio de ganancia en integrante."""

	if request.method == 'POST':
		form = MovimientoGananciaForm(request.POST)		

		if form.is_valid():			
			cambio = form.save(commit=False)
			cambio.responsable_id = request.user.id			
			cambio.save()
			return redirect('base.views.inicio')
	else:
		form = MovimientoGananciaForm(request.user)	

	return render_to_response('cambio_ganancia.html', {'formulario': form}, context_instance=RequestContext(request))

@login_required
def depositos(request):
	"""Vista para la gestión de los depositos de un integrante"""

	depositos = Deposito.objects.filter(integrante_id=request.user.id).order_by('-fecha_realizacion')
	total_aportado = 0

	for d in depositos:
		total_aportado += d.monto	

	fecha_inicio = date(2012,10,31)
	fecha_actual = date.today()
	meses = (fecha_actual - fecha_inicio).days / 30	
	aporte = meses*50
	deuda = aporte - total_aportado

	meses_deuda = 0

	if deuda > 0:
		meses_deuda = deuda / 50.0

	if request.method == 'POST':
		form = DepositoForm(request.POST)
		if form.is_valid():
			deposito = form.save(commit=False)
			deposito.integrante_id = request.user.id
			deposito.save()
			return redirect('base.views.depositos')
		else:
			return render_to_response('depositos.html', 
				{'depositos': depositos, 'form': form,
				'deuda': deuda, 'meses_deuda': meses_deuda, 
				'meses': meses, 'total_aportado': total_aportado}, 
				context_instance=RequestContext(request))
	
	form = DepositoForm()

	return render_to_response('depositos.html', 
		{'depositos': depositos, 'form': form, 
		'deuda': deuda, 'meses_deuda': meses_deuda, 
		'meses': meses, 'total_aportado': total_aportado}, 
		context_instance=RequestContext(request))

@login_required
def estado_ganancias(request):
	"""Información sobre el estado de ganancias de proyecto Medic"""

	integrantes = Integrante.objects.exclude(estado__in=['S','R'])
	total_asignado = 0

	for i in integrantes:
		total_asignado += i.ganancia

	retenido = 100 - total_asignado

	return render_to_response('estado_ganancias.html',
		{'integrantes': integrantes, 'retenido': retenido},
		context_instance=RequestContext(request) )