from models import *
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User


class IntegranteInline(admin.StackedInline):
    model = Integrante
    can_delete = False
    verbose_name_plural = 'integrantes'

# Modelos de administrador para las tablas de Base

class UsuarioAdmin(UserAdmin):
    inlines = (IntegranteInline, )

class MovimientosGananciasAdmin(admin.ModelAdmin):
	list_display = ('integrante', 'tipo', 'motivo')

class DepositoAdmin(admin.ModelAdmin):
	list_display = ('integrante', 'monto', 'num_operacion', 'fecha_realizacion', 'fecha_registro')

class CitaAdmin(admin.ModelAdmin):
	list_display = ('cita', 'autor', 'enviado_por' ,'veces_usado', 'ultima_vez')


# Se registra un nuevo usuario administrador.

admin.site.unregister(User)
admin.site.register(User, UsuarioAdmin)
admin.site.register(GananciaMovimiento, MovimientosGananciasAdmin)
admin.site.register(Deposito, DepositoAdmin)
admin.site.register(Cita, CitaAdmin)
