#encoding:utf-8

""" 
	Procesadores de contexto para Base.
"""

from models import Integrante

def integrante(request):

	""" El resultado de esta función es agregado a todo contexto procesado. 
		Siempre que se procese una plantilla se le será agreagado la información
		del integrante, siempre que esté logeado.
	"""

	if hasattr(request, 'user'):

		try:
			integrante = Integrante.objects.get(user_id=request.user.id)
		except:
			integrante = None
			
		return {'integrante': integrante}