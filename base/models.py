#encoding:utf-8

from django.db.models.signals import post_save

from django.db import models
from django.contrib.auth.models import User
from django.core.mail import send_mail

from django.utils.encoding import smart_unicode

from datetime import datetime
from datetime import date

BASE_APP_LABEL = 'base'

class Integrante(models.Model):
	
	"""
	Datos generales de un integrante del proyecto.
	"""
    
	EQUIPOS = (
		('D', "Desarrollo"),
		('M', 'Marketing'),
		('A', 'Administración')
	)

	CARGOS = (
		('L', 'Líder de equipo'),
		('G', 'Líder de proyecto'),
		('I', 'Int. de equipo')
	)

	ESTADOS = (
		('A', 'Activo'),
		('R', 'Retirado'),
		('S', 'Suspendido'),
		('E', 'Inactivo')
	)

	user = models.OneToOneField(User)

	cargo = models.CharField(max_length=1,
		choices=CARGOS,
		help_text='Cargo del integrante en el proyecto.')

	equipo = models.CharField(max_length=1, 
		choices= EQUIPOS, 
		help_text='Equipo al que pertenece el integrante')

	ganancia = models.FloatField(editable=False, default=0.0, help_text='Ganancia actual del integrante')

	foto = models.ImageField(max_length=25,
		upload_to='integrantes')

	estado = models.CharField(max_length=1, 
		choices=ESTADOS,
		help_text='Estado actual del integrante.')
	
	def __unicode__(self):
		return u"%s %s" % (self.user.first_name , self.user.last_name)

	# Métodos de apoyos para plantillas

	def nm_cargo(self):
		"""Nombre del cargo del integrante."""

		for c in self.CARGOS:
			if c[0] == self.cargo:
				return c[1]
		return None

	def nm_equipo(self):
		"""Nombre del equipo del integrante."""

		for e in self.EQUIPOS:
			if e[0] == self.equipo:
				return e[1]
		return None

	# Métodos complementarios

	def es_lider(self):
		"""Indica si es líder de prouyecto o de equipo."""

		if ( self.cargo == 'L' or self.cargo == 'G'):
			return True
		else:
			return False


	# Sobreescritura del método save de la clase.

	def save(self, *args, **kwargs):
		"""Operación necesaria para la integranción con la tabla de User."""

		if not self.pk:
			try:
				p = Integrante.objects.get(user=self.user)
				self.pk = p.pk
			except Integrante.DoesNotExist:
				pass

		super(Integrante, self).save(*args, **kwargs)


class GananciaMovimiento(models.Model):
	"""
	Cambios en la ganancia de un integrante realizado por algun líder, según las reglas
	estipuladas en la organización.
	"""

	class Meta:
		db_table = BASE_APP_LABEL + '_ganancia_movimiento'
		verbose_name=u'Cambio en ganancia'
		verbose_name_plural=u'Cambios en ganancia'

	TIPOS = (
		('I', 'Incremento'),
		('D', 'Decremento')
	)

	cambio = models.FloatField(max_length=2, 
		default=0.0,
		help_text='Cambio en la ganancia')	

	fecha_registro = models.DateField(auto_now_add=True, editable=False, default=date.today())

	tipo = models.BooleanField(help_text='Seleccionar para especificar un incremento de ganancia, de lo contrario será un decremento.')

	integrante = models.ForeignKey('Integrante', help_text='Miembro a quien se le realizará el cambio.')

	motivo = models.TextField(max_length=130,
		help_text='Motivo por el que se realiza el cambio en la ganancia.')

	responsable = models.ForeignKey('Integrante', related_name='responsable')

	def __unicode__(self):
		res = self.integrante.user.first_name + ", " 

		if (self.tipo):
			res += "+"
		else:
			res += "-"

		res += str(self.cambio)

		return res

	# Sobreescritura de métodos

	def save( self, *args, **kwargs):
		"""Cambio en la ganancia de un integrante y envío de correo de notificación."""		
	
		super(GananciaMovimiento, self).save(*args, **kwargs)

		if (self.tipo == True):
			self.integrante.ganancia = self.integrante.ganancia + self.cambio
		else:
			self.integrante.ganancia = self.integrante.ganancia - self.cambio

		self.integrante.save()

		mensaje = "Hola " + self.integrante.user.first_name
		mensaje += ", tu ganancia en Proyecto Medic ha sido modificada con un "

		if (self.tipo):
			mensaje += "incremento "
		else:
			mensaje += "decremento "

		mensaje += "de " 
		mensaje += str(self.cambio) 
		mensaje += "% por el motivo de: "
		mensaje += self.motivo
		mensaje += ". Este cambio ha sido realizado por el "
		mensaje += smart_unicode(self.responsable.nm_cargo())
		mensaje += ", "
		mensaje += self.responsable.user.first_name
		mensaje += " "
		mensaje += self.responsable.user.last_name
		mensaje += "."

		send_mail('Medic: Cambio en ganancia', mensaje, 
			'cpinelly@gmail.com', [self.integrante.user.email], fail_silently=False)

class Deposito(models.Model):
    
    num_operacion = models.CharField(max_length=14, help_text='Código de la operación realizada.', verbose_name=u'Código de operación')    
    monto = models.IntegerField(help_text='Monto aportado.')
    fecha_realizacion = models.DateField(default=datetime.now(), help_text='Fecha en que se realizó la operación.')
    fecha_registro = models.DateField(auto_now_add=True,editable=False)
    integrante = models.ForeignKey('Integrante')
    revisado = models.BooleanField(help_text='Especifique si el deposito ha sido revisado como válido.',default=False)
    def __unicode__(self):
        return self.num_operacion + "(Bs." + str(self.monto) +")"

    class Meta:
    	ordering = ['-fecha_registro']


class Cita(models.Model):

	cita = models.TextField(help_text='Cita o pensamiento')
	autor = models.CharField(max_length=80,
		blank=True,
		default="Anónimo",
		help_text='Autor de la cita. Si lo dejas en blanco se manajara como Anonimo.')

	fecha_registro = models.DateField(auto_now_add=True, editable=False)
	veces_usado = models.IntegerField(default=0, editable=False, help_text='Veces que se ha usado el campo')
	ultima_vez = models.DateField(editable=False, 
		null=True,
		help_text='Ultima vez que fue usado.')

	enviado_por = models.ForeignKey(User, blank=True)	

	def __unicode__(self):
		return "Cita de " + self.autor

# Señales

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Integrante.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)


