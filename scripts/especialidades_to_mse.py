# -*- coding:utf-8 -*-

import export_context
from export_context import UPDATE

from datamark.models import Especialidad
from datamark_mdb.models import EspecialidadMSE

print "Importado entorno y modelos"

especialidades = Especialidad.objects.all()

for e in especialidades:
	if UPDATE:
 		e.save(update=True)
 	else:
 		e.save()

print "Listo"