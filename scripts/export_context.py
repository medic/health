# -*- coding:utf-8 -*-

import os 
import sys

# Actualizacion de campos en MSE
ARG_UPDATE = '--update'   

# Exportación de toda la data a MSE. Se actualizalos el mse_id en las tablas de Datamark
ARG_EXPORT = '--export'   

UPDATE = False

if ( ARG_UPDATE in sys.argv ):
	UPDATE = True

base = os.path.dirname(os.path.realpath(__file__))
health_path = base[0:base.rfind('/')]

if (health_path == base[0:-1]):
	health_path = base[0:base.rfind('\\')]    # Posiblemente estemos en Windows


sys.path.append(health_path)

# ----------------------------------------------------------------------

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "health.settings")