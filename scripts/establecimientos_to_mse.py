# -*- coding:utf-8 -*-

import export_context
from export_context import UPDATE

from datamark.models import Establecimiento
from datamark_mdb.models import EstablecimientoMSE

print "Importado entorno y modelos"

establecimientos = Establecimiento.objects.all()

for e in establecimientos:
	if UPDATE:
 		e.save(update=True, not_revision=True)
 	else:
 		e.save(not_revision=True)

print "Listo"