# -*- coding:utf-8 -*-

import export_context
from export_context import UPDATE

from datamark.models import Doctor, Establecimiento
from datamark_mdb.models import EntityMSE

from django.core.exceptions import ObjectDoesNotExist

print "Importado entorno y modelos"

entidades = EntityMSE.objects.all()

for d in entidades:

	query = None
	nuevo = None

	if d.entity_type == 'D':
		query = Doctor.objects
		nuevo = Doctor()
	elif d.entity_type == 'E':
		query = Establecimiento.objects
		nuevo = Establecimiento()

	try:
		e = query.get(usuario=d.usuario)
	except ObjectDoesNotExist:
		print "La entidad %s no esta en Datamark" % (e.usuario)
		
		nuevo.from_mse_model(d)
		nuevo.save()
		nuevo.related_data_from_mse_model(d)


print "Listo"