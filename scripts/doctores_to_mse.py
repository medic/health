# -*- coding:utf-8 -*-

import export_context
from export_context import UPDATE

from datamark.models import Doctor
from datamark_mdb.models import DoctorMSE

print "Importado entorno y modelos"

doctores = Doctor.objects.all()

for e in doctores:
	if UPDATE:
 		e.save(update=True, not_revision=True)
 	else:
 		e.save(not_revision=True)

print "Listo"